all:
	echo -n

run:
	python/bin/python manage.py runserver 0.0.0.0:8000 --nothreading

runsecure:
	python manage.py runserver_plus 0.0.0.0:8000 --cert ../keys/stunnel.pem

st:
	python/bin/python manage.py collectstatic --noinput

install:
	[ -d python ] || virtualenv python
	python/bin/pip install -r requirements.txt
	[ -f "project/local_settings.py" ] || cp project/local_settings.py.sample project/local_settings.py
	[ -d data ] || mkdir data
	# bower install

test:
	py.test -n 2 apps/

cov:
	py.test -v -n 2 --cov-report term-missing --cov-config .coveragerc --cov apps/ apps/

pep8:
	py.test --pep8 apps/

playbook:
	ansible-playbook -l devservers -i inventory  playboook.yml

createdev:
	ssh-keygen -f ~/.ssh/known_hosts -R 192.168.33.10
	vagrant destroy -f
	vagrant up
	make playbook

update:
	[ -d static/css ] || rm -rf static/css
	[ -d static/js ] || rm -rf static/js
	echo 'yes' | python manage.py collectstatic
