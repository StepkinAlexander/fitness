from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.shortcuts import render


urlpatterns = patterns('',
    url(r'^$', 'apps.exercises.views.promo', name='promo'),
    url(r'^vk$', 'apps.accounts.views.home_vk_view', name='home_vk'),
    # url(r'^vk/$', 'apps.exercises.views.promo_vk', name='promo_vk'),

    url(r'^rules/$', 'apps.exercises.views.rules', name='rules'),
    url(r'^rules/vk/$', 'apps.exercises.views.rules_vk', name='rules_vk', kwargs={'SSL':True}),
    url(r'^promo/$', 'apps.exercises.views.promo', name='promo'),
    url(r'^promo/vk/$', 'apps.exercises.views.promo_vk', name='promo_vk', kwargs={'SSL':True}),
    url(r'^about/$', 'apps.exercises.views.about', name='about'),
    url(r'^about/vk/$', 'apps.exercises.views.about_vk', name='about_vk', kwargs={'SSL':True}),

    url(r'^', include('apps.accounts.urls', namespace='accounts')),
    url(r'^', include('apps.exercises.urls', namespace='exercises')),
    url(r'^', include('apps.diets.urls', namespace='diets')),
    url(r'^', include('apps.progress.urls', namespace='progress')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^html/(?P<template_name>.*\.html)$', (lambda request, template_name: render(request, template_name))),
    )


handler404 = 'apps.accounts.views.handle404'
handler500 = 'apps.accounts.views.handle500'
