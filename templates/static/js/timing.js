$(function () {
    $(window).load(function () {

        // 300000 - ??? 5 ?????
        var timeNow = Date.now();


        if (localStorage.getItem("timeStart")) {
            if ((localStorage.getItem("timeStart") - timeNow) >= 299000) {
                localStorage.setItem("timeStart", timeNow);
            }
        } else {
            localStorage.setItem("timeStart", timeNow);
        }


        setInterval(function () {
            var timeRange = calcTimeRange();
            if (timeRange >= 299000) {
                sendTime(timeRange);
            }
        }, 300000);

        document.addEventListener("visibilitychange", checkVisibility);
        window.addEventListener('beforeunload', function () {
            sendTime(calcTimeRange());
        });

    });

    function calcTimeRange () {
        var timeEnd = Date.now();
        return (timeEnd - localStorage.getItem("timeStart"));
    }

    function sendTime (timeRange) {
        localStorage.setItem( "timeStart", Date.now());
        $.ajax({
            url: '/api/online/set/',
            type: 'get',
            data: {
                time: timeRange/(60*1000)
            }
        });
    }

    function checkVisibility() {
        if (document.hidden) {
            sendTime(calcTimeRange());
        } else {
            localStorage.setItem( "timeStart", Date.now());
        }
    }
});