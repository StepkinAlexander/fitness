$(window).load(function(){

    setTimeout(function () {
        $('#popup1').show();
    }, 5000);
});

function updateScale(last_day) {
    $('.shkala_block').each(function() {
        if(!last_day) {
            last_day = $(this).attr('attr');
        }

        var selector = '';
        for(var i=1; i<=14; i++) {
            if (selector == '') {
                selector = '.l_' + i;
            } else {
                selector += ', .l_' + i
            };
            if($(this).attr('attr') == i) {
                $(selector).fadeIn(0);
            }
        };
        $('.blue_line').addClass('day_' + $(this).attr('attr'));
        $('.blue_line a.blue_lady').addClass('day_' + last_day + '_lady')
    });
};

$(document).ready(function(){
    // updateScale();

    // pause/play on video
    // $('video').on('click', function(){
    //     if(this.paused) {
    //         this.play()
    //     } else {
    //         this.pause()
    //     }
    // })

    // sound update
    function mute(change) {
        if (!localStorage.getItem("mute")){
            localStorage.setItem("mute", "false");
        }

        var mute = localStorage.getItem("mute");
        if(change) {
            if(mute == 'true'){
                localStorage.setItem("mute", "false");
            } else {
                localStorage.setItem("mute", "true");
            }
        }

        if(localStorage.getItem("mute") == 'true') {
            $('#dynamic').text('Включить звук');
            $('video').prop('muted', true);
            ga('send', 'sound', 'off');
        } else {
            $('#dynamic').text('Выключить звук')
            $('video').prop('muted', false)
        }
    };

    mute(false);
    $('#dynamic').on('click', function(){
        mute(true);
    })
    // ende

    // RadioButton
    $('.radioblock').find('.radio').each(function(){
        $(this).click(function(){
            // Заносим текст из нажатого дива в переменную
            var valueRadio = $(this).html();
            // Находим любой активный переключатель и убираем активность
            $(this).parent().find('.radio').removeClass('active');
            // Нажатому диву добавляем активность
            $(this).addClass('active');
            // Заносим значение объявленной переменной в атрибут скрытого инпута
            $(this).parent().find('input').val(valueRadio);
        });
    });

    // Checkbox
// Отслеживаем событие клика по диву с классом check
    $('.checkboxes').find('.check').click(function(){
        // Пишем условие: если вложенный в див чекбокс отмечен
        if( $(this).find('input').is(':checked') ) {
            // то снимаем активность с дива
            $(this).removeClass('active');
            // и удаляем атрибут checked (делаем чекбокс не отмеченным)
            $(this).find('input').removeAttr('checked');
            $('.mr_7').addClass('grays_t');
            $('.mr_7').attr('disabled',true);

            // если же чекбокс не отмечен, то
        } else {
            // добавляем класс активности диву
            $(this).addClass('active');
            // добавляем атрибут checked чекбоксу
            $(this).find('input').attr('checked', true);
            $('.mr_7').removeClass('grays_t');
            $('.mr_7').removeAttr('disabled');
        }
    });


    //Действия по умолчанию
    $(".tab_content").hide(); //скрыть весь контент
    $("ul.tabs li:first").addClass("active").show().find('.hidd_arra').fadeIn(0); //Активировать первую вкладку
    $(".tab_content:first").show(); //Показать контент первой вкладки

    //Событие по клику
    $("ul.tabs li").click(function() {
        $("ul.tabs li").removeClass("active"); //Удалить "active" класс
        $('.hidd_arra').fadeOut(0);
        $(this).find('.hidd_arra').fadeIn(0);
        $(this).addClass("active"); //Добавить "active" для выбранной вкладки
        $(".tab_content").hide(); //Скрыть контент вкладки
        var activeTab = $(this).find("a").attr("href"); //Найти значение атрибута, чтобы определить активный таб + контент
        $(activeTab).fadeIn(); //Исчезновение активного контента
        return false;
    });

    //Действия по умолчанию
    $(".tab_contents").hide(); //скрыть весь контент
    $("ul.tabss li:first").addClass("active").show(); //Активировать первую вкладку
    $(".tab_contents:first").show(); //Показать контент первой вкладки

    //Событие по клику
    $("ul.tabss li").click(function() {
        $("ul.tabss li").removeClass("active"); //Удалить "active" класс
        $(this).addClass("active"); //Добавить "active" для выбранной вкладки
        $(".tab_contents").hide(); //Скрыть контент вкладки
        var activeTab = $(this).find("a").attr("href"); //Найти значение атрибута, чтобы определить активный таб + контент
        $(activeTab).fadeIn(); //Исчезновение активного контента
        return false;
    });

    $(function()
    {
        $('.scroll-pane').jScrollPane(
            {
                verticalDragMinHeight: 21,
                verticalDragMaxHeight: 21,
                horizontalDragMinWidth: 21,
                horizontalDragMaxWidth: 21
            }
        );
    });

    $(function()
    {
        $('.scroll-paned').jScrollPane(
            {
                verticalDragMinHeight: 21,
                verticalDragMaxHeight: 21,
                horizontalDragMinWidth: 21,
                horizontalDragMaxWidth: 21
            }
        );
    });

    $(function()
    {
        $('.scroll-pane2').jScrollPane(
            {
                verticalDragMinHeight: 21,
                verticalDragMaxHeight: 21,
                horizontalDragMinWidth: 21,
                horizontalDragMaxWidth: 21
            }
        );
    });


    $('.hovers').mouseover(function(){
        $('.speach').stop().fadeOut(0);
        $(this).find('.speach').stop().fadeIn(1000);
    });
    $('.hovers').mouseout(function(){
        $('.speach').stop().fadeOut(0);
    });


    $('#mycarousel_shema_proesda').jcarousel({
        scroll: 1,
        wrap: "circular",
        show:3
    });


    $('.closes').click(function() {
        $('.popups').hide();
    });

    $('.open_popup').click(function() {
        var popup_id = $('#' + $(this).attr("rel"));
        $(popup_id).show();
        $('.overlay').show();
    });

    $('.popup .close, .overlay').click(function() {
        $('.overlay, .popup').hide();
    });

    $(".show_inp").on('click', function(){
        $(".show_inp").removeClass('active_show_inp');
        $(this).addClass('active_show_inp');
        $(".hidd_b").slideUp(200);
        $(this).parents('.link_blocks').find('.hidd_b').slideDown(200);
    });
    // }, function(){
    //     $(this).removeClass('active_show_inp');
    //     $(this).parents('.link_blocks').find('.hidd_b').slideUp(200);
    // });

    // $(".ft").click(function() {
    //     return false;
    // })

    $("#profile_avatar").on("change", function(){
        $('#profile_form').submit();
    })

    $.ajax({
        type: 'GET',
        url: "/api/statements/",
    }).done(function(res) {
        $('#statements').text(res.statements)
    });


    // if($("#motivation_video").length){
    //     var controls = {
    //         video: $("#motivation_video"),
    //         playpause: $("#playpause")
    //     };

    //     var video = controls.video[0];

    //     controls.playpause.click(function(){
    //         if (video.paused) {
    //             video.play();
    //             $(this).text("Pause");
    //         } else {
    //             video.pause();
    //             $(this).text("Play");
    //         }

    //         $(this).toggleClass("paused");
    //     });

    //     video.addEventListener("canplay", function() {
    //         video.play();

    //         $('#dynamic').click(function(){
    //             var classes = this.getAttribute("class");
    
    //             if (new RegExp('\\boff\\b').test(classes)) {
    //                 classes = classes.replace(" off", "");
    //             } else {
    //                 classes = classes + " off";
    //             }

    //             this.setAttribute("class", classes);

    //             video.muted = !video.muted;
    //         })

    //     }, false);
    // }

    $('input').on('focus', function(){
        var form_block = $(this).parents('.link_blocks');
        $('.shadow_alert', form_block).hide();
    });

     if ($('#wrapper_carousel_shema_proesda').length) {
        $('#tab11').data('view', 'yes');

        $('.jcarousel-next, .jcarousel-prev').on('click', function () {
            var $activeItem = $('.jcarousel-item.active');
            var indexOfActiveItem = parseInt($activeItem.attr('jcarouselindex'));
            var $nextActive;

            if($(this).hasClass('jcarousel-next')) {
                $nextActive = $('[jcarouselindex=' + (indexOfActiveItem + 1) + ']');
            } else {
                $nextActive = $('[jcarouselindex=' + (indexOfActiveItem - 1) + ']');
            }

            $nextActive.click();
            var $tabBlocks = $('.tab_contents');
            var showingBlockLink = $('a', $nextActive).attr('href');

            $(showingBlockLink).data('view', 'yes');
            var viewedNumber = 0;
            
            for (var i = 0; i < $tabBlocks.length; i++) {
                if ($tabBlocks.eq(i).data('view') === 'yes') {
                    viewedNumber++;
                }
            }
            if (viewedNumber === $tabBlocks.length) {
                $.ajax({
                    type: 'GET',
                    url: "/api/diet/viewed/",
                })
            }
            ga('send', 'eating', 'switching');
        });
    };

    if ($('.right_form_block_register').length) {
        var $form = $('.right_form_block_register');

        var $fields = $("input", $form);
        var $button = $('.send_sbm', $form);

        var $weight = $('[name="weight"]', $form);
        var $age = $('[name="age"]', $form);
        var $growth = $('[name="growth"]', $form);
        var $email = $('[name="email"]', $form);
        var $checkbox = $('.check', $form);


        var fieldSet = [$weight, $age, $growth];
        var pattern = /^[0-9]+$/i;
        var emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

        var fillFields = false;
        var numbersParametrs = false;
        var correctParametrs = false;
        var emailCorrected = false;


        $form.parents('form').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/',
                type: 'post',
                data: $(this).serialize(),
                success: function (response) {
                    var message;
                    if (response.error) {
                        showPopup(response.error);
                    } else {
                        showPopup(response.info);
                        setTimeout(function () {
                            $('#link_trainers')[0].click();
                        }, 3000);
                    }
                },
                error: function () {
                    showPopup('Ой-ой! У нас что-то сломалось, попробуй позже!');
                }
            });
        });

        $fields.on('keyup', function () {checkFields(this)} );
        $checkbox.on('click', function () {checkFields(this)});

        function checkFields  (field) {
            var counterFilled = 0;

            /*check that all fields is filled*/
            for (var i = 0; i < $fields.length; i++) {
                var fieldType = $fields.eq(i).attr('type');
                if (fieldType === "text" || fieldType === "email" || fieldType === "password") {
                    if ($fields.eq(i).val()) {
                        counterFilled++;
                    }
                } else if (fieldType === "checkbox") {
                    if ($fields.eq(i).prop('checked')) {
                        counterFilled++
                    }
                } else {
                    counterFilled++;
                }
            }
            if (counterFilled === $fields.length) {
                fillFields = true;
            } else {
                fillFields = false
            }


            /*check that weight, age and growth is number*/
            var fieldValue = $(field).val();
            var counterNumberfield = 0;

            if (field.name === "weight" || field.name === "age" || field.name === "growth") {
                if (fieldValue !== '' && !pattern.test(fieldValue)) {
                    $(field).addClass('hasError');
                } else {
                    $(field).removeClass('hasError');
                }
            }

            fieldSet.forEach(function (item) {
                var itemFieldValue = $(item).val();
                if (pattern.test(itemFieldValue)) {
                    counterNumberfield++;
                }
            });
            if (counterNumberfield === 3) {
                numbersParametrs = true;
            } else {
                numbersParametrs = false;
            }


            /*check e-mail corrected*/

            if (fieldValue !== '' && field.name === "email") {
                if(emailPattern.test(fieldValue)){
                    $(field).removeClass('hasError');
                    emailCorrected = true;
                } else {
                    emailCorrected = false;
                }
            }

            check();
        }
        $($email).on('focusout', function () {
            var fieldValue = $(this).val();
            if (fieldValue === '') {return false;}

            if(emailPattern.test(fieldValue)){
                $(this).removeClass('hasError');
                emailCorrected = true;
            } else {
                $(this).addClass('hasError');
                showPopup('Введите корректное значение');
                emailCorrected = false;
            }
            check();
        });

        $($weight).add($growth).add($age).on('focusout', function () {
            checkParameter(this);
        });

        function checkParameter  (field) {
            var counter = 0;
            var thisName = field.name;
            fieldSet.forEach(function (item) {
                var fieldValue = $(item).val();
                if (!pattern.test(fieldValue)) {
                    correctParametrs = false;
                    return false;
                }
                if ($(item).attr('name') == "weight") {
                    if (fieldValue < 40 || fieldValue > 200) {
                        if (thisName =="weight") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }

                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }

                if ($(item).attr('name') == "age") {
                    if (fieldValue > 90) {
                        if (thisName == "age") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }

                    } else if (fieldValue < 18) {
                        if (thisName == "age") {
                            showPopup('Принять участие в акции могут только совершенолетние пользователи');
                            $(item).addClass('hasError');
                        }
                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }

                if ($(item).attr('name') == "growth") {
                    if (fieldValue < 130 || fieldValue > 220) {
                        if (thisName == "growth") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }

                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }
            });

            if (counter === 3) {
                correctParametrs = true;
            } else {
                correctParametrs = false;
            }

            check();
        }

        function check () {
            if (fillFields && numbersParametrs && correctParametrs && emailCorrected) {
                $button.prop('disabled', false);
            } else {
                $button.prop('disabled', true);
            }
        }
    }

    if ($('.right_anketa_block_edit').length) {
        $form = $('.right_anketa_block_edit');

        $fields = $("input", $form);
        $button = $('.blue_btn_sky', $form);

        $weight = $('[name="weight"]', $form);
        $age = $('[name="age"]', $form);
        $growth = $('[name="growth"]', $form);

        fieldSet = [$weight, $age, $growth];
        pattern = /^[0-9]+$/i;

        fillFields = false;
        numbersParametrs = false;
        correctParametrs = false;

        $form.parents('form').on('submit', function () {
            showPopup("Изменения сохранены");
            ga('send', 'registration', 'done');
        });


        $fields.on('keyup', function () {
            var counterFilled = 0;

            /*check that all fields is filled*/
            for (var i = 0; i < $fields.length; i++) {
                var fieldType = $fields.eq(i).attr('type');
                if (fieldType === "text" || fieldType === "email" || fieldType === "password") {
                    if ($fields.eq(i).val()) {
                        counterFilled++;
                    }
                } else if (fieldType === "checkbox") {
                    if ($fields.eq(i).prop('checked')) {
                        counterFilled++
                    }
                } else {
                    counterFilled++;
                }
            }
            if (counterFilled === $fields.length) {
                fillFields = true;
            } else {
                fillFields = false
            }


            /*check that weight, age and growth is number*/
            var fieldValue = $(this).val();
            var counterNumberfield = 0;

            if (this.name === "weight" || this.name === "age" || this.name === "growth") {
                if (fieldValue !== '' && !pattern.test(fieldValue)) {
                    $(this).addClass('hasError');
                } else {
                    $(this).removeClass('hasError');
                }
            }

            fieldSet.forEach(function (item) {
                var itemFieldValue = $(item).val();
                if (pattern.test(itemFieldValue)) {
                    counterNumberfield++;
                }
            });
            if (counterNumberfield === 3) {
                numbersParametrs = true;
            } else {
                numbersParametrs = false;
            }

            checkAnketsParameter();
            checkAnkete();
        });
        
        $($weight).add($growth).add($age).on('focusout', function () {
            checkAnketsParameter(this);
        });

        function checkAnketsParameter  (field) {
            var counter = 0;
            var thisName = field.name;
            fieldSet.forEach(function (item) {
                var fieldValue = $(item).val();
                if (!pattern.test(fieldValue)) {
                    correctParametrs = false;
                    return false;
                }
                if ($(item).attr('name') == "weight") {
                    if ((fieldValue < 40 || fieldValue > 200)) {
                        if (thisName =="weight") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }
                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }

                if ($(item).attr('name') == "age") {
                    if (fieldValue > 90) {
                        if (thisName == "age") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }

                    } else if (fieldValue < 18) {
                        if (thisName == "age") {
                            showPopup('Принять участие в акции могут только совершенолетние пользователи');
                            $(item).addClass('hasError');
                        }
                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }

                if ($(item).attr('name') == "growth") {
                    if (fieldValue < 130 || fieldValue > 220) {
                        if (thisName == "growth") {
                            showPopup('Введите корректное значение');
                            $(item).addClass('hasError');
                        }

                    } else {
                        $(item).removeClass('hasError');
                        counter++;
                    }
                }
            });

            if (counter === 3) {
                correctParametrs = true;
            } else {
                correctParametrs = false;
            }

            checkAnkete();
        }

        function checkAnkete () {
            if (fillFields && numbersParametrs && correctParametrs) {
                $button.prop('disabled', false);
            } else {
                $button.prop('disabled', true);
            }
        }
    }

    function showPopup (message) {
        $('#popup5 .register').html(message);
        $('#popup5').show();
        $('.overlay').show();
    }
});
