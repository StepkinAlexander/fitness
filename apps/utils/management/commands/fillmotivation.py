# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer, MotivationVideo


def add_movie(trainer, url):
    try:
        movie = MotivationVideo.objects.get(trainer=trainer, video_url=url)
    except MotivationVideo.DoesNotExist:
        movie = MotivationVideo(trainer=trainer, video_url=url)
    movie.save()


class Command(BaseCommand):

    def handle(self, *args, **options):

        print 'Загружаю данные о мотивационных роликах'
        motivation_videos = {
            '1': [
                '/static/video/balagur/train_1_03.mp4',
                '/static/video/balagur/train_1_04.mp4',
                '/static/video/balagur/train_1_05.mp4',
                '/static/video/balagur/train_1_06.mp4',
                '/static/video/balagur/train_3_01.mp4',
                '/static/video/balagur/train_3_02.mp4',
                '/static/video/balagur/train_3_03.mp4',
                '/static/video/balagur/train_3_04.mp4',
                '/static/video/balagur/train_3_05.mp4',
                '/static/video/balagur/train_3_06.mp4',
                '/static/video/balagur/train_4_01.mp4',
                '/static/video/balagur/train_4_02.mp4',
                '/static/video/balagur/train_4_03.mp4',
            ],
            '2': [
                '/static/video/macho/train_1_07.mp4',
                '/static/video/macho/train_3_01.mp4',
                '/static/video/macho/train_3_02.mp4',
                '/static/video/macho/train_3_03.mp4',
                '/static/video/macho/train_3_04.mp4',
                '/static/video/macho/train_3_05.mp4',
                '/static/video/macho/train_3_06.mp4',
                '/static/video/macho/train_3_07.mp4',
                '/static/video/macho/train_4_01.mp4',
                '/static/video/macho/train_4_02.mp4',
                '/static/video/macho/train_4_03.mp4',
            ],
            '3': [
                '/static/video/yog/train_1_07.mp4',
                '/static/video/yog/train_1_08.mp4',
                '/static/video/yog/train_3_01.mp4',
                '/static/video/yog/train_3_02.mp4',
                '/static/video/yog/train_3_03.mp4',
                '/static/video/yog/train_3_04.mp4',
                '/static/video/yog/train_3_05.mp4',
                '/static/video/yog/train_3_06.mp4',
                '/static/video/yog/train_3_07.mp4',
                '/static/video/yog/train_4_01.mp4',
                '/static/video/yog/train_4_02.mp4',
                '/static/video/yog/train_4_03.mp4',
            ],
        }

        for trainer_id in motivation_videos.keys():
            trainer = Trainer.objects.get(id=trainer_id)
            movies = motivation_videos.get(trainer_id)
            for movie in movies:
                add_movie(trainer, movie)
