# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.progress.models import Statement


def recalc():
    stats = Statement.objects.all()
    num=1
    for stat in stats:
        stat.number = num
        stat.save()
        num += 1
    print 'Done'


class Command(BaseCommand):

    def handle(self, *args, **options):

        # Пересчёт номеров заявок
        print 'Пересчёт номеров заявок'
        recalc()
