# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer, Exercise, ExercisesDayIntro
from apps.diets.models import Diet
from apps.accounts.models import User
from apps.progress.models import ExerciseProgress

from random import randint


def create_progress(username, exercise_id, sets, repetitions):
    user = User.objects.get(username=username)
    progress = ExerciseProgress(user=user, exercise_id=exercise_id)
    progress.sets = sets
    progress.repetitions = repetitions
    progress.save()


def set_random_online_time():
    from random import randint
    users = User.objects.all()
    for user in users:
        user.minutes_online = randint(100, 200)
        user.save()


def create_day_introes(trainer_id, day, before='#', after='#'):
    trainer = Trainer.objects.get(id=trainer_id)
    intro = ExercisesDayIntro(trainer=trainer, day=day)
    intro.before = before
    intro.after = after
    intro.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        call('python manage.py resetdb', shell=True)

        trainers_quantity = 3
        days = 7
        weeks = 2
        exercises_quantity_in_day = 4

        for trainer_id in range(1, trainers_quantity + 1):
            for day in range(1, days + 1):
                create_day_introes(trainer_id, day)

        for i in range(1, 25):
            create_progress('admin', randint(1, 64), randint(1, 5), randint(1, 5))

        set_random_online_time()

