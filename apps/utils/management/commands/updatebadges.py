# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.progress.models import Badge


def update_badge(code, name, description='', share_text='', icon_vkid=''):
    try:
        badge = Badge.objects.get(code=code, name=name)
    except Badge.DoesNotExist:
        badge = Badge(code=code, name=name)
    badge.description = description
    badge.share_text = share_text
    badge.icon_vkid = icon_vkid
    badge.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        # создаём бейджи
        print 'Создаём бейджи'
        update_badge(
            'pioneer',
            'Первооткрыватель',
            'После одного дня пребывания в приложении',
            'Позади первый день тренировок!',
            'photo66802348_363447880',
        )
        update_badge(
            'purposeful',
            'Целеустремлённая',
            'После 2х дней подряд пребывания в приложении',
            'Ты на пути к цели! Первые два дня позади!',
            'photo66802348_363447882',
        )
        update_badge(
            'seeker',
            'Искательница',
            'После 3х дней подряд пребывания в приложении',
            'Три тренировки подряд. Так держать!',
            'photo66802348_363447880',
        )
        update_badge(
            'victress',
            'Победительница',
            'После 4х дней подряд пребывания в приложении',
            'Всего четыре дня фитнеса, а ты почти у цели выиграть поездку на Бали!',
            'photo66802348_363447884',
        )
        update_badge(
            'celebrity',
            'Знаменитость',
            'После 7ми дней подряд пребывания в приложении',
            'Твои старания будут вознаграждены!',
            'photo66802348_363447885',
        )
        update_badge(
            'legend',
            'Легенда',
            'После 14ти дней подряд пребывания в приложении',
            'Ты — легенда! Скоро ты узнаешь, поедешь ли ты на Бали',
            'photo66802348_363447887',
        )

        update_badge(
            'cheerleader',
            'Болельщица',
            'За просмотр программы питания в течение двух любых дней',
            'За просмотр программы питания в течение двух любых дней',
            'photo66802348_363447892',
        )
        update_badge(
            'fitnes_master',
            'Фитнес Мастер',
            'За просмотр страницы с упражнениями. (Нужно собрать 2 часа просмотра страниц упражнений)',
            'Больше 2-х часов тренировок!',
            'photo66802348_363447899',
        )
        update_badge(
            'record_holder',
            'Рекордсменка',
            'За просмотр 10 видеозаписей упражнений до конца',
            'Вот это результат!',
            'photo66802348_363447906',
        )
        update_badge(
            'fitnes_guru',
            'Фитнес Гуру',
            'За сжигание 700 калорий',
            'На пути к идеальной фигуре!',
            'photo66802348_363447914',
        )
        update_badge(
            'champion',
            'Чемпионка',
            'За сжигание 2000 калорий за всё время',
            'Награда за усердия — стройная фигура',
            'photo66802348_363447918',
        )
        update_badge(
            'fitnes_abonement',
            'Фитнес абонемент',
            'За шару результата дня в соцсетях',
            'Распрощалась сегодня с XXX калориями',
            'photo66802348_363447920',
        )
