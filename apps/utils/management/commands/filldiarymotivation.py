# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer, MotivationDiaryVideo


def add_motivation_video(trainer, url):
    try:
        movie = MotivationDiaryVideo.objects.get(trainer=trainer, video_url=url)
    except MotivationDiaryVideo.DoesNotExist:
        movie = MotivationDiaryVideo(trainer=trainer, video_url=url)
    movie.save()


class Command(BaseCommand):

    def handle(self, *args, **options):

        print 'Загружаю данные о мотивационных роликах для дневника'
        motivation_videos = {
            '1': [
                '/static/video/balagur/joke_1_01.mp4',
                '/static/video/balagur/joke_1_02.mp4',
                '/static/video/balagur/joke_1_03.mp4',
                '/static/video/balagur/joke_1_04.mp4',
            ],
            '2': [
                '/static/video/macho/joke_1_01_2.mp4',
                '/static/video/macho/joke_1_02_2.mp4',
                '/static/video/macho/joke_1_03_2.mp4',
            ],
            '3': [
                '/static/video/yog/day_1_04_1.mp4',
                '/static/video/yog/day_1_05_1.mp4',
                '/static/video/yog/day_1_06_1.mp4',
            ],
        }

        for trainer_id in motivation_videos.keys():
            trainer = Trainer.objects.get(id=trainer_id)
            movies = motivation_videos.get(trainer_id)
            for movie in movies:
                add_motivation_video(trainer, movie)
