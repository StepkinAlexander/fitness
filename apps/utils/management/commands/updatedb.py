# coding: utf-8
import _mysql
from subprocess import call
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError


class Command(BaseCommand):

    def handle(self, *args, **options):
        call('python manage.py migrate', shell=True)

        call('python manage.py updatebadges', shell=True)
