# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer, MotivationDietVideo
from apps.diets.models import Diet


def add_motivation_video(trainer, url, is_main=False):
    try:
        movie = MotivationDietVideo.objects.get(trainer=trainer, video_url=url)
    except MotivationDietVideo.DoesNotExist:
        movie = MotivationDietVideo(trainer=trainer, video_url=url)
    movie.is_main = is_main
    movie.save()


class Command(BaseCommand):

    def handle(self, *args, **options):

        print 'Загружаю данные о диет-рликах'
        videos = {
            '1': '/static/video/balagur/diet_1_01.mp4',
            '2': '/static/video/macho/diet_1_01_3.mp4',
            '3': '/static/video/yog/diet_1_01_1.mp4',

        }

        print 'Загружаю данные о мотивационных роликах диет'
        motivation_videos = {
            '1': [
                '/static/video/balagur/train_4_01.mp4',
                '/static/video/balagur/train_4_02.mp4',
                '/static/video/balagur/train_4_03.mp4',
            ],
            '2': [
                '/static/video/macho/train_4_01.mp4',
                '/static/video/macho/train_4_02.mp4',
                '/static/video/macho/train_4_03.mp4',
            ],
            '3': [
                '/static/video/yog/train_4_01.mp4',
                '/static/video/yog/train_4_02.mp4',
            ],
        }

        for trainer_id in motivation_videos.keys():
            trainer = Trainer.objects.get(id=trainer_id)
            movies = motivation_videos.get(trainer_id)
            add_motivation_video(trainer, videos.get(trainer_id), True)
            for movie in movies:
                add_motivation_video(trainer, movie)
