# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.accounts.models import User
from apps.progress.views import rating_tender


class Command(BaseCommand):

    def handle(self, *args, **options):

        print 'Пересчёт заявок'
        users = User.objects.filter(id__gt=1)
        for user in users:
            rating_tender(user)
        print 'Закончено'
