# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer


def update_trainer(id, name, image_url, about='', methodic='', video_url=''):
    try:
        trainer = Trainer.objects.get(id=id)
    except Trainer.DoesNotExist:
        trainer = Trainer.objects.create(id=id)
    trainer.name = name
    trainer.image = image_url
    trainer.about_trainer = about
    trainer.about_methodic = methodic
    trainer.video_intro_url = video_url
    trainer.save()


class Command(BaseCommand):

    def handle(self, *args, **options):

        # создаём/редактируем тренеров
        print 'Создаём тренеров'
        abouts = [
            '48 г цельных злаков в день поддерживают твое здоровье, ведь в них содержатся необходимые витамины и минеральные вещества',
            'Красота и стройная фигура не требуют жертв. Занимайся всего 15 минут вместе с Nestlé Fitness и соблюдай наши рекомендации по питанию!',
            'Любовь к себе начинается с уверенности! Присоединяйся к программе 14 дней, очищай организм с помощью цельных злаков и покупай платье на один размер меньше уже к этому лету',
        ]
        video_intro = [
            '/static/video/balagur/train_info1_5_2.mp4',
            '/static/video/macho/train_info3_2_2.mp4',
            '/static/video/yog/train_info3.mp4',
        ]
        update_trainer(1, 'balagur', '/static/images/chel_1.png', '', abouts[0], video_intro[0])
        update_trainer(2, 'macho', '/static/images/chel_2.png', '', abouts[1], video_intro[1])
        update_trainer(3, 'yog', '/static/images/chel_3.png', '', abouts[2], video_intro[2])
