# coding: utf-8
import _mysql
from subprocess import call
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import OperationalError
from django.db import connection

from apps.exercises.models import Trainer, ExercisesDayIntro


def create_day_introes(trainer_id, day, intro={}):
    trainer = Trainer.objects.get(id=int(trainer_id))
    try:
        obj = ExercisesDayIntro.objects.get(trainer=trainer, day=day)
    except ExercisesDayIntro.DoesNotExist:
        obj = ExercisesDayIntro(trainer=trainer, day=day)
    obj.before = intro.get('before')
    obj.after = intro.get('after')
    obj.save()


class Command(BaseCommand):

    def handle(self, *args, **options):

        trainers_quantity = 3
        days = 7
        intros = {
            '1': {
                'before': '/static/video/balagur/train_1_01.mp4',
                'after': '/static/video/balagur/day_1_01.mp4',
            },
            '2': {
                'before': '/static/video/macho/train_1_02_3.mp4',
                'after': '/static/video/macho/train_1_07.mp4',
            },
            '3': {
                'before': '/static/video/yog/train_1_02_2.mp4',
                'after': '/static/video/yog/day_1_01_1.mp4',
            },
        }

        for trainer_id in intros.keys():
            for day in range(1, days + 1):
                create_day_introes(trainer_id, day, intros.get(trainer_id))
