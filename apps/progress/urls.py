from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.progress.views',
    url(r'^api/trainer/$', 'get_trainer', name='trainer'),
    url(r'^api/trainer/day/$', 'get_trainer_day_api', name='trainer_day'),
    url(r'^api/trainers/$', 'get_trainers', name='trainers'),
    url(r'^api/trainer/exercises/$', 'get_trainer_exercises', name='trainer_exercises'),
    url(r'^api/online/set/$', 'set_time_online', name='time_online'),
    url(r'^api/video/viewed/$', 'video_viewed', name='video_viewed'),
    url(r'^api/diet/viewed/$', 'diet_viewed', name='diet_viewed'),
    url(r'^api/burned/$', 'get_burned_calories', name='burned_calories'),
    url(r'^api/share/$', 'share_result', name='share_result'),
    url(r'^api/statements/$', 'get_statements', name='get_statements'),
    url(r'^api/winner/$', 'get_winner', name='get_winner'),
    url(r'^api/trainer/reset/$', 'trainer_reset', name='trainer_reset'),

    url(r'^anketa/$', 'anketa_view', name='anketa'),
    url(r'^anketa/vk/$', 'anketa_view_vk', name='anketa_vk'),

    url(r'^diary/$', 'progress_exercise', name='progress_exercise'),
    url(r'^diary/vk/$', 'progress_exercise_vk', name='progress_exercise_vk'),

    url(r'^rating/$', 'rating', name='rating'),
    url(r'^rating/vk/$', 'rating_vk', name='rating_vk'),
    # url(r'^progress/exercise/$', 'progress_exercise', name='progress_exercise'),
)