# coding: utf-8
import json
from datetime import datetime, timedelta
from decimal import Decimal

from django.conf import settings
from django.contrib import messages
from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.views.generic import View
from django.db.utils import IntegrityError

from apps.accounts.views import get_day_user_in_system
from apps.accounts.models import User
from apps.exercises.models import Trainer, Exercise, MotivationDiaryVideo, Advice
from apps.diets.models import Diet, DietViewed
from .models import ExerciseProgress, Badge, Achievement, Statement
from .forms import ProgressForm

DEFAULT_TRAINER = settings.DEFAULT_TRAINER


def get_trainers(request):
    # params: no params
    trainers = Trainer.objects.all()
    if not request.user.username:
        return JsonResponse({'trainers': list(trainers.values())})

    day = get_day_user_in_system(request.user)
    trainers_days = {}
    percents = [0, 7, 14, 21, 28, 35, 43, 50, 57, 64, 71, 78, 85, 93, 100]
    for trainer in trainers:
        days = 0
        for d in range(1, day + 1):
            exercises = Exercise.objects.filter(trainer=trainer, day=d)
            progresses = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises, repetitions__gt=0)
            if len(progresses) > 1:
                days += 1
        html = ''
        for i in range(1, days + 1):
            html += '<li class="tic active_tic"></li>'
        for i in range(days + 1, 15):
            html += '<li class="tic passive_tic"></li>'

        trainers_days[str(trainer.id)] = {
            'html': html,
            'percent': percents[days]
        }

    return JsonResponse({
        'trainers': list(trainers.values()),
        'days': trainers_days,
    })


def get_trainer(request):
    # params: trainer_id
    trainer_id = request.GET.get('id')
    if not id:
        return JsonResponse({'error': 'no trainer id'})

    try:
        trainer = Trainer.objects.get(id=trainer_id)
    except Trainer.DoesNotExist:
        return JsonResponse({'error': 'trainer does not exist'})
    else:
        return JsonResponse({'trainer': model_to_dict(trainer)})


def get_trainer_day(request, trainer):
    if request.user.username:
        user = request.user
    else:
        user_id = request.GET.get('user_id') if request.GET.get('user_id') else 1
        user = User.objects.get(id=user_id)
    day = get_day_user_in_system(user)
    trainer_day = 0
    for d in range(1, day + 1):
        exercises = Exercise.objects.filter(trainer=trainer, day=d)
        progress = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
        if len(progress) > 1:
            trainer_day = d

    exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)
    progress = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
    if progress:  # проверка что последняя запись в дневнике не сегодня
        if progress[len(progress) - 1].action_time.day != datetime.now().day:
            trainer_day += 1

    if trainer_day == 0:
        trainer_day = 1
    elif trainer_day > day:
        trainer_day = day
    return trainer_day


def get_trainer_day_api(request):
    # params: trainer_id
    trainer_id = request.GET.get('trainer_id')
    if not id:
        return JsonResponse({'error': 'no trainer id'})

    try:
        trainer = Trainer.objects.get(id=trainer_id)
        trainer_day = get_trainer_day(request, trainer)
    except Trainer.DoesNotExist:
        return JsonResponse({'error': 'trainer does not exist'})
    else:
        return JsonResponse({'trainer_day': trainer_day})


def get_trainer_exercises(request):
    # params: trainer_id
    trainer_id = request.GET.get('trainer_id', DEFAULT_TRAINER)
    try:
        int(trainer_id)
    except ValueError:
        trainer_id = DEFAULT_TRAINER
    except:
        return JsonResponse({'error': 'no trainer id'})

    try:
        trainer = Trainer.objects.get(id=trainer_id)
        if 'last_day' in request.GET:
            day = request.GET.get('last_day')
        else:
            day = get_trainer_day(request, trainer)
        exercises = Exercise.objects.filter(trainer=trainer, day=day).all()
    except Trainer.DoesNotExist:
        return JsonResponse({'error': 'trainer does not exist'})
    else:
        return JsonResponse({'exercises': list(exercises.values())})


def get_burned_calories_json(request):
    day = get_day_user_in_system(request.user)

    try:
        progresses = ExerciseProgress.objects.filter(user=request.user)
    except:
        progresses = []

    daily = 0
    total = 0
    result = {}
    for progress in progresses:
        total += float(progress)
    result['total'] = int(total + 0.5)

    if 'last_day' in request.GET:
        exercises = Exercise.objects.filter(day=request.GET.get('last_day'))
        progresses = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
        daily = 0
        for progress in progresses:
            daily += float(progress)
        result['daily'] = int(daily + 0.5)
    else:
        to_date = datetime.now()
        from_date = to_date.replace(hour=0, minute=0, second=0, microsecond=0)
        progresses = ExerciseProgress.objects.filter(user=request.user, action_time__gt=from_date)
        result['daily'] = int(daily_burned_by_trainer(request))

    today = 0
    for progress in progresses:
        today += float(progress)
    result['today'] = int(today + 0.5)
    return result


def get_burned_calories(request):
    return JsonResponse(get_burned_calories_json(request))


def record_badge(user, badge=None):
    if badge:
        achievement = Achievement(user=user, badge=badge)
        achievement.save()


def rating_tender(user):
    def save_statement(user, statement_type):
        try:
            statement = Statement(user=user, statement_type=statement_type)
            statements_quantity = len(Statement.objects.all())
            statement.number = statements_quantity
            statement.save()
        except:
            pass

    # если наступил День Че(в конфиге) заявки больше не принимаются
    day_che = datetime.strptime(settings.DAY_CHE, "%d.%m.%Y")
    if datetime.now() >= day_che:
        return

    # подать заявку(автомат)
    statements = Statement.objects.filter(user=user)
    if len(statements) >= 6:
        return
    statements_quantity = len(statements)

    trainers = Trainer.objects.all()

    # 1-4 дни
    for i in range(4):
        day = 'day' + str(i + 1)
        try:
            statement = Statement.objects.get(user=user, statement_type=day)
        except Statement.DoesNotExist:
            exercises = Exercise.objects.filter(day=i + 1, trainer=trainers[0])
            progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
            if len(progresses) > 1:
                save_statement(user, day)
            else:
                exercises = Exercise.objects.filter(day=i + 1, trainer=trainers[1])
                progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
                if len(progresses) > 1:
                    save_statement(user, day)
                else:
                    exercises = Exercise.objects.filter(day=i + 1, trainer=trainers[2])
                    progresses = ExerciseProgress.objects.filter(user=user, exercise__in=exercises)
                    if len(progresses) > 1:
                        save_statement(user, day)
        except:
            pass

    # каждые 5 бейджей
    try:
        achievements = Achievement.objects.filter(user=user)
        if len(achievements) >= 5:
            try:
                statement = Statement.objects.get(user=user, statement_type='5badges')
            except Statement.DoesNotExist:
                save_statement(user, '5badges')
            except:
                pass

            if len(achievements) >= 10:
                try:
                    statement = Statement.objects.get(user=user, statement_type='5badges2')
                except Statement.DoesNotExist:
                    save_statement(user, '5badges2')
                except:
                    pass
    except Achievement.DoesNotExist:
        pass


def recalc_achievements(user, is_share=False):
    # пользователь зашарил результат дня
    if is_share:
        badge = Badge.objects.get(code='fitnes_abonement')
        try:
            achievement = Achievement.objects.get(user=user, badge=badge)
        except Achievement.DoesNotExist:
            record_badge(user, badge)

            # сгенерировать заявки
            rating_tender(user)
        return

    progresses = ExerciseProgress.objects.filter(user=user, sets__gt=0, repetitions__gt=0)
    if len(progresses) > 1:
        days = {}
        for progress in progresses:
            day = progress.exercise.day
            if days.get(str(day)):
                days[str(day)] += 1
            else:
                days[str(day)] = 1

        filtered = []

        def filter(k, v):
            if v >= 2:
                filtered.append(int(k))
        trash = map(lambda i: filter(i, days[i]), days)
        filtered.sort()

        maxchain = 1
        chain = 1
        i = 1
        while i < len(filtered):
            if filtered[i - 1] + 1 == filtered[i]:
                chain += 1
            else:
                if chain > maxchain:
                    maxchain = chain
                chain = 0
            i += 1
        if chain > maxchain:
            maxchain = chain

        # после 1го дня
        if maxchain > 0:
            badge = Badge.objects.get(code='pioneer')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

        # после 2х дней подряд
        if maxchain > 1:
            badge = Badge.objects.get(code='purposeful')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

        # после 3х дней подряд
        if maxchain > 2:
            badge = Badge.objects.get(code='seeker')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

        # после 4х дней подряд
        if maxchain > 3:
            badge = Badge.objects.get(code='victress')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

        # после 7ми дней подряд
        if maxchain > 6:
            badge = Badge.objects.get(code='celebrity')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

        # после 14ти дней подряд
        if maxchain > 13:
            badge = Badge.objects.get(code='legend')
            try:
                achievement = Achievement.objects.get(user=user, badge=badge)
            except Achievement.DoesNotExist:
                achievement = Achievement(user=user, badge=badge)
                achievement.save()

    # просмотреть программу питания в течение 2 любых дней
    badge = Badge.objects.get(code='cheerleader')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        try:
            progress = DietViewed.objects.filter(user=user)
        except DietViewed.DoesNotExist:
            pass
        else:
            if len(progress) > 1:
                record_badge(user, badge)

    # заниматься больше 2х часов
    badge = Badge.objects.get(code='fitnes_master')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        if user.minutes_online > 120:
            record_badge(user, badge)

    # просмотреть до конца 10 упражнений за всё время
    badge = Badge.objects.get(code='record_holder')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user, video_viewed=True)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            if len(progress) >= 10:
                badge = Badge.objects.get(code='record_holder')
                record_badge(user, badge)

    # сожжено всего 700 калорий
    badge700 = Badge.objects.get(code='fitnes_guru')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge700)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            sum = 0
            for p in progress:
                sum += float(p)
            if sum > 700:
                record_badge(user, badge700)

    # сожжено всего 2к калорий
    badge2k = Badge.objects.get(code='champion')
    try:
        achievement = Achievement.objects.get(user=user, badge=badge2k)
    except Achievement.DoesNotExist:
        try:
            progress = ExerciseProgress.objects.filter(user=user)
        except ExerciseProgress.DoesNotExist:
            pass
        else:
            sum = 0
            for p in progress:
                sum += float(p)
            if sum > 2000:
                record_badge(user, badge2k)

    rating_tender(user)
    return


def set_time_online(request):
    # params: user_id[, time]
    user = request.user
    if not user:
        return JsonResponse({'error': 'no user'})
    time = request.GET.get('time', 0)

    user.minutes_online += Decimal(time)
    user.save()

    recalc_achievements(user)

    return JsonResponse({'time': 'setted'})


def video_viewed(request):
    # просмотры упражнений
    # params: user_id, exercise_id[, done]
    user = request.user

    if not user.username:
        try:
            user = User.objects.get(id=request.POST.get('user_id'))
        except User.DoesNotExist:
            return JsonResponse({'error': 'wrong user id'})

    exercise_id = request.GET.get('exercise_id')
    if not exercise_id:
        return JsonResponse({'error': 'no exercise id'})
    try:
        exercise = Exercise.objects.get(id=exercise_id)
    except Exercise.DoesNotExist:
        return JsonResponse({'error': 'wrong exercise id'})

    try:
        exercise_viewed = ExerciseProgress.objects.get(exercise=exercise, user=user)
        if exercise_viewed.video_viewed:
            recalc_achievements(user)
            return JsonResponse({'video': 'viewed=0'})
    except ExerciseProgress.DoesNotExist:
        exercise_viewed = ExerciseProgress(exercise=exercise, user=user)
    done = request.GET.get('done', 0)
    origin = done
    done = True if done > 0 else False
    exercise_viewed.video_viewed = done
    if exercise_viewed.sets + exercise_viewed.repetitions == 0:
        exercise_viewed.action_time = datetime.now() - timedelta(days=1)
    exercise_viewed.save()

    recalc_achievements(user)

    return JsonResponse({'video': 'viewed=%s' % origin})


def diet_viewed(request):
    # params: user_id, diet_id[, done]
    user = request.user
    if not user.username:
        try:
            user = User.objects.get(id=request.POST.get('user_id'))
        except User.DoesNotExist:
            return JsonResponse({'error': 'wrong user id'})

    day = get_day_user_in_system(request.user) % 14 + 1
    try:
        diet = Diet.objects.get(day=day)
    except User.DoesNotExist:
        return JsonResponse({'error': 'something wrong. reload page and repeat'})

    try:
        diet_viewed = DietViewed.objects.get(diet=diet, user=user)
    except DietViewed.DoesNotExist:
        diet_viewed = DietViewed(diet=diet, user=user)
    done = request.POST.get('done', 0)
    origin = done
    done = True if done > 0 else False
    diet_viewed.viewed = done
    diet_viewed.save()

    recalc_achievements(user)

    return JsonResponse({'diet': 'viewed=%s' % done})


def share_result(request):
    # params: user_id
    user = request.user
    if not user.username:
        try:
            user = User.objects.get(id=request.POST.get('user_id'))
        except User.DoesNotExist:
            return JsonResponse({'error': 'wrong user id'})

    recalc_achievements(user, is_share=True)

    return JsonResponse({'share': '1'})


def diary_context(request):
    context = {
        'user': request.user,
        'day': get_day_user_in_system(request.user),
        'vkappid': settings.SOCIAL_AUTH_VK_OAUTH2_KEY,
        'site_url': settings.SITE_URL,
    }

    return context


def daily_burned_by_trainer(request):
    trainer_id = request.GET.get('trainer_id')
    if not trainer_id:
        trainer_id = settings.DEFAULT_TRAINER
    trainer = Trainer.objects.get(id=trainer_id)
    trainer_day = get_trainer_day(request, trainer)

    exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)
    progresses = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)

    weight = request.user.weight
    burned = 0
    for progress in progresses:
        burned += progress.sets * progress.repetitions * progress.exercise.repetition_calories_burned

    burned = burned * weight
    return burned


class DiaryView(View):

    def get(self, request):
        trainer_id = request.GET.get('trainer_id')
        context = diary_context(request)

        if trainer_id:
            trainer = Trainer.objects.get(id=trainer_id)
            context['trainer'] = trainer
        else:
            trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
            context['default'] = trainer

        day = get_day_user_in_system(request.user)
        trainer_day = get_trainer_day(request, trainer)
        context['trainer_day'] = trainer_day
        if 'last_day' in request.GET:
            last_day = request.GET.get('last_day')
            context['last_day'] = last_day
            exercises = Exercise.objects.filter(trainer=trainer, day=last_day)

            if last_day > 7:
                adv_day = last_day - 7
            else:
                adv_day = last_day
            advice = Advice.objects.get(day=adv_day)
        else:
            exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)

            if trainer_day > 7:
                adv_day = trainer_day - 7
            else:
                adv_day = trainer_day
            advice = Advice.objects.get(day=adv_day)
        context['advice'] = advice

        forms = []
        index = 0
        for exercise in exercises:
            try:
                progress = ExerciseProgress.objects.get(user=request.user, exercise=exercise)
            except ExerciseProgress.DoesNotExist:
                progress = ExerciseProgress(user=request.user, exercise=exercise)
            progress_form = ProgressForm(instance=progress, prefix='ex' + str(index))
            forms.append(progress_form)
            index += 1
        context['forms'] = forms

        videos = MotivationDiaryVideo.objects.filter(trainer=trainer)
        context['videos'] = videos
        context['share_url'] = settings.SITE_SHARE_URL

        return TemplateResponse(request, 'diary.html', context)

    def post(self, request):
        form = ProgressForm(request.POST)
        if form.is_valid():
            sets = form.cleaned_data['sets']
            try:
                int(sets)
            except ValueError:
                return JsonResponse({'error': 'вводите числа'})

            reps = form.cleaned_data['repetitions']
            try:
                int(reps)
            except ValueError:
                return JsonResponse({'error': 'вводите числа'})

            if sets > 0 and reps > 0:
                trainer_id = request.GET.get('trainer_id')
                exercise_id = request.GET.get('exercise_id')
                user = request.user

                try:
                    exercise = Exercise.objects.get(id=exercise_id)
                except Exercise.DoesNotExist:
                    return JsonResponse({'error': 'ошибка в упражнении, обновите страницу и повторите'})
                else:
                    if int(sets) >= 2 * exercise.prescription_sets:
                        return JsonResponse({'error': 'превышено значение'})

                    if int(reps) >= 2 * exercise.prescription_repetitions:
                        return JsonResponse({'error': 'превышено значение'})

                try:
                    progress = ExerciseProgress.objects.get(user=request.user, exercise=exercise)
                except ExerciseProgress.DoesNotExist:
                    progress = ExerciseProgress(user=request.user, exercise=exercise)
                progress.sets = sets
                progress.repetitions = reps
                progress.action_time = datetime.now()
                progress.save()

                recalc_achievements(user)
            else:
                return JsonResponse({'result': 'данные невведены или нулевые'})

        return JsonResponse({'result': 'сохранено'})


progress_exercise = DiaryView.as_view()


class DiaryVkView(View):

    def get(self, request, **kwargs):
        trainer_id = request.GET.get('trainer_id')
        context = diary_context(request)

        if trainer_id:
            trainer = Trainer.objects.get(id=trainer_id)
            context['trainer'] = trainer
        else:
            trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
            context['default'] = trainer

        day = get_day_user_in_system(request.user)
        trainer_day = get_trainer_day(request, trainer)
        context['trainer_day'] = trainer_day
        if 'last_day' in request.GET:
            last_day = request.GET.get('last_day')
            context['last_day'] = last_day
            exercises = Exercise.objects.filter(trainer=trainer, day=last_day)

            if last_day > 7:
                adv_day = last_day - 7
            else:
                adv_day = last_day
            advice = Advice.objects.get(day=adv_day)
        else:
            exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)

            if trainer_day > 7:
                adv_day = trainer_day - 7
            else:
                adv_day = trainer_day
            advice = Advice.objects.get(day=adv_day)
        context['advice'] = advice

        forms = []
        index = 0
        for exercise in exercises:
            try:
                progress = ExerciseProgress.objects.get(user=request.user, exercise=exercise)
            except ExerciseProgress.DoesNotExist:
                progress = ExerciseProgress(user=request.user, exercise=exercise)
            progress_form = ProgressForm(instance=progress, prefix='ex' + str(index))
            forms.append(progress_form)
            index += 1
        context['forms'] = forms

        videos = MotivationDiaryVideo.objects.filter(trainer=trainer)
        context['videos'] = videos

        context.update({
            'vk': True,
            'vk_share_url': settings.VK_SHARE_URL,
            'burned': get_burned_calories_json(request),
        })
        return TemplateResponse(request, 'diary.html', context)

    def post(self, request):
        form = ProgressForm(request.POST)
        if form.is_valid():
            sets = form.cleaned_data['sets']
            try:
                int(sets)
            except ValueError:
                return JsonResponse({'error': 'вводите числа'})

            reps = form.cleaned_data['repetitions']
            try:
                int(reps)
            except ValueError:
                return JsonResponse({'error': 'вводите числа'})

            if sets > 0 and reps > 0:
                trainer_id = request.GET.get('trainer_id')
                exercise_id = request.GET.get('exercise_id')
                user = request.user

                try:
                    exercise = Exercise.objects.get(id=exercise_id)
                except Exercise.DoesNotExist:
                    return JsonResponse({'error': 'ошибка в упражнении, обновите страницу и повторите'})
                else:
                    if int(sets) >= 2 * exercise.prescription_sets:
                        return JsonResponse({'error': 'превышено значение'})

                    if int(reps) >= 2 * exercise.prescription_repetitions:
                        return JsonResponse({'error': 'превышено значение'})

                try:
                    progress = ExerciseProgress.objects.get(user=request.user, exercise=exercise)
                except ExerciseProgress.DoesNotExist:
                    progress = ExerciseProgress(user=request.user, exercise=exercise)
                progress.sets = sets
                progress.repetitions = reps
                progress.save()

                recalc_achievements(user)
            else:
                return JsonResponse({'result': 'данные невведены или нулевые'})

        return JsonResponse({'result': 'сохранено'})


progress_exercise_vk = DiaryVkView.as_view()


def get_statements(request):
    from django.http import JsonResponse

    if request.user.username:
        statements = Statement.objects.filter(user=request.user)
    else:
        statements = []
    return JsonResponse({'statements': len(statements)})


def anketa_context(request):
    all_badges = Badge.objects.all()

    own_badges = []
    if request.user.username:
        achievements = Achievement.objects.filter(user=request.user)

        counter = 0
        if len(achievements) >= 10:
            limit = 10
        elif len(achievements) >= 5:
            limit = 5
        else:
            limit = 0

        for arch in achievements:
            counter += 1
            badge = { 'code': arch.badge.code }

            if arch.badge.code in ['pioneer', 'purposeful', 'seeker', 'victress']:
                badge['sended'] = True
            elif counter <= limit:
                badge['sended'] = True

            own_badges.append(badge)

    context = {
        'user': request.user,
        'day': get_day_user_in_system(request.user),
        'badges': {
            'all': all_badges,
            'own': json.dumps(own_badges),
        },
        'share_static': settings.SITE_SHARE_STATIC,
    }

    return context


def anketa_view(request):
    context = anketa_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'anketa.html', context)


def anketa_view_vk(request, **kwargs):
    context = anketa_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'anketa.html', context)


def rating_context(request):
    if request.user.username:
        rating_tender(request.user)

    trainer_id = request.GET.get('trainer_id')
    context = {}
    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer
    else:
        trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
        context['default'] = trainer

    if request.user.username:
        statements = Statement.objects.filter(user=request.user)
        context['statements_total'] = len(statements)

    users = User.objects.filter(id__gt=1)
    ratings = {}
    for user in users:
        stats = Statement.objects.filter(user=user)
        ratings[user.id] = len(stats)

    import operator
    sorted_ratings = sorted(ratings.items(), key=operator.itemgetter(1), reverse=True)
    ratings_out = []
    for id, stats in sorted_ratings:
        user = User.objects.get(id=id)
        ratings_out.append({
            'user': user,
            'stats': stats,
            'day': get_day_user_in_system(user),
        })

    context.update({
        'user': request.user,
        'day': get_day_user_in_system(request.user),
        'rating_table_data': ratings_out,
        'site_url': settings.SITE_URL,
    })
    return context


def rating(request):
    context = rating_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'rating.html', context)


def rating_vk(request, **kwargs):
    context = rating_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'rating.html', context)


def get_winner(request):
    statements = Statement.objects.all()
    statements_total = len(statements)

    fat_burned_total = 0
    progresses = ExerciseProgress.objects.all()
    for progress in progresses:
        weight = progress.user.weight
        exercise = progress.exercise
        fat_burned = weight * progress.sets * progress.repetitions * exercise.repetition_calories_burned
        fat_burned_total += fat_burned

    winned_statement = int(fat_burned_total) % statements_total
    if winned_statement == 0:
        winned_statement = statements_total / 2

    statement = Statement.objects.get(number=winned_statement)
    return JsonResponse({
        'id': winned_statement,
        'winner': '%s\n%s' % (statement.user.first_name, statement.user.last_name),
    })


def trainer_reset(request):
    if not request.user.username:
        return JsonResponse({'error': 'user not authorized'})

    if not 'trainer_id' in request.GET:
        return JsonResponse({'error': 'no trainer'})
    trainer_id = request.GET.get('trainer_id')
    trainer = Trainer.objects.get(id=trainer_id)

    exercises = Exercise.objects.filter(trainer=trainer)
    progresses = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
    for progress in progresses:
        progress.delete()
    return JsonResponse({'status': 'ok'})
