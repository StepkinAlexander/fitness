# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0004_auto_20150413_1604'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='statement',
            options={'ordering': ['id'], 'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430 \u043d\u0430 \u0443\u0447\u0430\u0441\u0442\u0438\u0435 \u0432 \u0440\u043e\u0437\u044b\u0433\u0440\u044b\u0448\u043a', 'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0443\u0447\u0430\u0441\u0442\u0438\u0435 \u0432 \u0440\u043e\u0437\u044b\u0433\u0440\u044b\u0448\u043a'},
        ),
    ]
