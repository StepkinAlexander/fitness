# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('progress', '0009_auto_20150425_2153'),
    ]

    operations = [
        migrations.CreateModel(
            name='TableLocking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table', models.CharField(max_length=255, verbose_name='\u0442\u0430\u0431\u043b\u0438\u0446\u0430')),
                ('action_time', models.DateTimeField(auto_now_add=True, verbose_name='\u043d\u0430\u0447\u0430\u043b\u043e \u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u043a\u0438')),
                ('need_update', models.BooleanField(default=False)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
