# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0008_auto_20150424_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='icon_vkid',
            field=models.CharField(default=' ', max_length=40, verbose_name='\u0438\u0434 \u0444\u043e\u0442\u043e \u0432 \u0412\u041a'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='exerciseprogress',
            name='action_time',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
