# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0003_auto_20150410_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='statement',
            name='statement_type',
            field=models.CharField(default=b'', max_length=25, verbose_name='\u0422\u0438\u043f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='badge',
            name='icon',
            field=models.ImageField(upload_to=b'badge_icon/%Y/%m/%d', max_length=255, verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430', blank=True),
            preserve_default=True,
        ),
    ]
