# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0011_auto_20150506_1621'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='achievement',
            unique_together=set([('user', 'badge')]),
        ),
    ]
