# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('exercises', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=20, verbose_name='\u041a\u043e\u0434')),
                ('icon', models.ImageField(upload_to=b'badgr_icon/%Y/%m/%d', max_length=255, verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430', blank=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0437\u0430\u0441\u043b\u0443\u0433\u0438', blank=True)),
            ],
            options={
                'ordering': ['id'],
                'verbose_name': '\u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0414\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExerciseProgress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sets', models.IntegerField(default=0, verbose_name='\u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u043f\u043e\u0434\u0445\u043e\u0434\u043e\u0432')),
                ('repetitions', models.IntegerField(default=0, verbose_name='\u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u043f\u043e\u0432\u0442\u043e\u0440\u043e\u0432')),
                ('video_viewed', models.BooleanField(default=False, verbose_name='\u0432\u0438\u0434\u0435\u043e \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u043d\u043e')),
                ('exercise', models.ForeignKey(to='exercises.Exercise')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0433\u0440\u0435\u0441\u0441 \u0432 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0438 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u041f\u0440\u043e\u0433\u0440\u0435\u0441\u0441\u044b \u0432 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0438 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
    ]
