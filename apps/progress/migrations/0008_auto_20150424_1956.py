# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0007_exerciseprogress_action_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exerciseprogress',
            name='action_time',
            field=models.DateTimeField(verbose_name='\u0432\u0440\u0435\u043c\u044f \u0448\u0430\u0440\u044b'),
            preserve_default=True,
        ),
    ]
