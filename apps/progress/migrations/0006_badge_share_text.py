# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0005_auto_20150413_1829'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='share_text',
            field=models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0434\u043b\u044f \u0448\u0430\u0440\u044b', blank=True),
            preserve_default=True,
        ),
    ]
