# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0006_badge_share_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='exerciseprogress',
            name='action_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 25, 19, 37, 51, 861657), verbose_name='\u0432\u0440\u0435\u043c\u044f \u0448\u0430\u0440\u044b', auto_now_add=True),
            preserve_default=False,
        ),
    ]
