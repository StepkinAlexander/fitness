# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0010_tablelocking'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='statement',
            unique_together=set([('user', 'statement_type')]),
        ),
    ]
