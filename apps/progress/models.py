# coding: utf-8
from django.db import models
from apps.exercises.models import Exercise, Trainer
from apps.accounts.models import User
from datetime import datetime, timedelta


class TableLocking(models.Model):
    user = models.ForeignKey(User)
    table = models.CharField(u'таблица', max_length=255)
    action_time = models.DateTimeField(u"начало блокировки", auto_now_add=True)
    need_update = models.BooleanField(default=False)


class ExerciseProgress(models.Model):
    exercise = models.ForeignKey(Exercise)
    user = models.ForeignKey(User)
    sets = models.IntegerField(u"выполнено подходов", default=0)
    repetitions = models.IntegerField(u"выполнено повторов", default=0)
    video_viewed = models.BooleanField(u"видео просмотрено", default=False)
    action_time = models.DateTimeField()

    def __unicode__(self):
        return unicode(self.user.weight * self.sets * self.repetitions * self.exercise.repetition_calories_burned)

    def __float__(self):
        return float(self.user.weight * self.sets * self.repetitions * self.exercise.repetition_calories_burned)

    class Meta:
        verbose_name = "Прогресс в выполнении упражнения"
        verbose_name_plural = "Прогрессы в выполнении упражнения"


class Badge(models.Model):
    code = models.CharField(u'Код', max_length=20)
    icon = models.ImageField(u'Иконка', upload_to='badge_icon/%Y/%m/%d', blank=True, max_length=255)
    name = models.CharField(u'Название', max_length=255)
    description = models.TextField(u'Описание заслуги', blank=True)
    share_text = models.TextField(u'Текст для шары', blank=True)
    icon_vkid = models.CharField(u'ид фото в ВК', max_length=40)

    def __unicode__(self):
        return unicode('[%s] %s' % (self.code, self.name))

    class Meta:
        verbose_name = "Бейдж за Достижение"
        verbose_name_plural = "Бейджи за Достижения"
        ordering = ['id']


class Achievement(models.Model):
    user = models.ForeignKey(User)
    badge = models.ForeignKey(Badge)
    received_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Достижение"
        verbose_name_plural = "Достижения"
        ordering = ['id']
        unique_together = (("user", "badge"),)


class ShareDay(models.Model):
    user = models.ForeignKey(User)
    trainer = models.ForeignKey(Trainer)
    day = models.IntegerField(u"день")
    action_time = models.DateTimeField(u"время шары", auto_now_add=True)


class Statement(models.Model):
    # заявка на участие в рейтинге
    number = models.IntegerField(u"номер заявки")
    user = models.ForeignKey(User)
    # day1, day2, day3, day4, 5badges, 5badges2(6-10)
    statement_type = models.CharField(u'Тип', max_length=25, default='')
    request_time = models.DateTimeField(u"время создания заявки", auto_now_add=True)

    class Meta:
        verbose_name = "Заявка на участие в розыгрышк"
        verbose_name_plural = "Заявки на участие в розыгрышк"
        ordering = ['id']
        unique_together = (("user", "statement_type"),)
