# coding: utf-8
from django import forms
from django.contrib.auth import get_user_model
from .models import User
from django.db import models


class UserForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = [
            'email',
            'first_name',
            'last_name',
            'age',
            'sex',
            'city',
            'weight',
            'growth',
            'avatar',
        ]


class RegistrationForm(forms.Form):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    email = forms.CharField(max_length=100)
    password = forms.CharField(max_length=100)
    city = forms.CharField(max_length=50)
    age = forms.CharField(max_length=50)
    sex = forms.CharField(max_length=50)
    weight = forms.CharField(max_length=50)
    growth = forms.CharField(max_length=50)

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(username=email).count():
            raise forms.ValidationError(u"Email уже зарегистрирован.")
        return email

    def clean_sex(self):
        if self.cleaned_data['sex'] == 'Мужской':
            sex = 'm'
        else:
            sex = 'f'
        return sex


class FeedbackForm(forms.Form):
    name = forms.CharField(max_length=50)
    message = forms.CharField(max_length=255)
