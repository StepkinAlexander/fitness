# coding: utf-8
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
# from django.contrib.sites.models import Site


admin.site.unregister(Group)
# admin.site.unregister(Defaut)
admin.site.register(get_user_model(), UserAdmin)
# admin.site.register(Site)
