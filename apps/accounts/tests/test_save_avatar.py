# coding: utf-8
import os
import pytest
from apps.accounts.models import get_avatar_from_social_response, save_avatar
from .test_fixtures import user


class TestGetAvatarFromSocialResponse:

    def test_facebook_provider(self):
        response = {
            'id': '111',
        }
        url = get_avatar_from_social_response('facebook', response)
        assert url == 'http://graph.facebook.com/111/picture?type=large'

    def test_vkontakte_provider(self):
        response = {
            'photo': 'http://cs621520.vk.me/321/SxfyugRkDFY.jpg'
        }
        url = get_avatar_from_social_response('vk-oauth2', response)
        assert url == 'http://cs621520.vk.me/321/SxfyugRkDFY.jpg'

    def test_odnoklassniki_provider(self):
        response = {
            'pic_2': 'http://cs621520.vk.me/123/SxfyugRkDFY.jpg'
        }
        url = get_avatar_from_social_response('odnoklassniki-oauth2', response)
        assert url == 'http://cs621520.vk.me/123/SxfyugRkDFY.jpg'


class TestSaveAvatar:

    @pytest.mark.django_db
    def test_save_avatar(self, monkeypatch, user):
        #TODO с тестом беда
        filename = 'apps/accounts/tests/fixtures/picture.jpg'
        content = open(filename, 'rb').read()
        response = type('Response', (object,), {'content': content, 'status_code': 200})

        monkeypatch.setattr('requests.get', lambda *a, **kw: response)
        backend = type('BackendMock', (object,), {'name': 'vk-oauth2'})

        save_avatar(backend, user, {
            'id': '12345',
        })
        original_size = os.path.getsize(filename)
        assert user.avatar.storage.exists(user.avatar.path)
        assert original_size == user.avatar.storage.size(user.avatar.path)

    @pytest.mark.django_db
    def test_save_avatar_ioerror(self, monkeypatch, user):

        def urlopen_mock(*a, **kw):
            raise IOError('test error')

        monkeypatch.setattr('requests.get', urlopen_mock)

        backend = type('BackendMock', (object,), {'name': 'vk-oauth2'})
        response = {
            'id': '12345',
        }
        save_avatar(backend, user, response)
        assert user.avatar.name == ''

    @pytest.mark.django_db
    def test_save_avatar_if_exists(self, monkeypatch, user):
        monkeypatch.delattr('requests.get')
        user.avatar = 'my.png'
        backend = type('BackendMock', (object,), {'name': 'vk-oauth2'})
        response = {
            'id': '12345',
        }
        save_avatar(backend, user, response)
        assert user.avatar.name == 'my.png'


class TestUserUpdateAvatar:

    @pytest.mark.django_db
    def test_update_avatar(self, user):
        assert user.avatar.name == ''
        user.update_avatar(url='')
        assert user.avatar.name == ''
