# coding: utf-8
import pytest
from django.contrib.auth import get_user_model


@pytest.yield_fixture
def user():
    username = 'vasya'
    password = 'qwerty'
    _user = get_user_model().objects.create_user(username=username, password=password)
    _user.password_raw = password
    yield _user
    _user.delete()
