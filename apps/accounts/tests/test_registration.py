# coding: utf-8
import json
import pytest
from django.core import mail
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from .test_fixtures import user


@pytest.mark.django_db
def test_registration(client):
    assert get_user_model().objects.all().count() == 0

    response = client.get(reverse('accounts:registration'))
    assert response.status_code == 200

    data = {
        'first_name': 'Ivan',
        'last_name': 'Sidorov',
        'email': 'ivan@sidorov.ru',
        'password': '123',
    }
    response = client.post(reverse('accounts:registration'), data=data, follow=True)

    assert response.status_code == 200
    user = get_user_model().objects.get(username=data['email'])
    assert not user.is_active
    assert user.first_name == data['first_name']
    assert user.last_name == data['last_name']

    mymail = mail.outbox[0]
    assert mymail.subject == 'Активация аккаунта.'
    activation_url = reverse('accounts:activation', args=[user.activation_key])
    assert activation_url in mymail.body


@pytest.mark.django_db
def test_registration_no_email(client):
    data = {
        'first_name': 'Ivan',
        'last_name': 'Sidorov',
    }
    response = client.post(reverse('accounts:registration'), data=data)
    assert response.status_code == 200
    assert 'error' in response.content


@pytest.mark.django_db
def test_registration_done(user, client):
    user.is_active = False
    user.save()
    response = client.get(reverse('accounts:activation', args=[user.activation_key]))
    assert response.status_code == 302
    user = get_user_model().objects.get(id=user.id)
    assert user.is_active
