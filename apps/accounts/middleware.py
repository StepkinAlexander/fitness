# coding: utf-8
from .models import User


class UsersCountMiddleware(object):

    def process_template_response(self, request, response):
        users = User.objects.filter(id__gt=1)
        response.context_data['registered_users'] = len(users)

        if request.user.username:
            response.context_data['user_age'] = request.user.age
            response.context_data['user_gender'] = request.user.sex

        return response
