# coding: utf-8
from django.conf import settings
from django.contrib import messages
from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.core.mail import send_mail
from django.http import JsonResponse, HttpResponseRedirect

from .models import User, generate_activation_code
from .forms import UserForm, RegistrationForm, FeedbackForm

from datetime import datetime

from django.contrib.auth import authenticate, login
from social.apps.django_app.utils import psa, load_strategy, load_backend
from social.exceptions import AuthTokenRevoked


def flush_messages(request):
    storage = messages.get_messages(request)
    for _ in storage:
        # print _
        pass


def home_vk_view(request):
    flush_messages(request)
    if 'access_token' in request.GET:
        backend = 'vk-oauth2'
        request.social_strategy = load_strategy(request)
        uri = '/'

        request.backend = load_backend(request.social_strategy, backend, uri)

        token = request.GET['access_token']
        try:
            user = request.backend.do_auth(token)
        except AuthTokenRevoked:
            user = None
            return redirect(reverse('promo_vk'))
        else:
            if user:
                login(request, user)
                if (user.weight == 0) or (user.growth == 0) or (user.age == 0):
                    return redirect(reverse('accounts:profile_vk'))
                else:
                    return redirect('https://%s/trainers/vk' % request.get_host())
            else:
                return redirect('https://%s/trainers/vk' % request.get_host())
    else:
        return redirect('https://%s/promo/vk' % request.get_host())


def logout_view(request):
    flush_messages(request)
    logout(request)
    return redirect('/')


class LoginBaseView(View):

    def try_login(self, request):
        flush_messages(request)
        username = request.POST['username']
        password = request.POST['password']

        if '@' in username:
            try:
                user = get_user_model().objects.get(username=username)
                username = user.username
            except get_user_model().DoesNotExist:
                pass

        error = None
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return
            else:
                error = u'Учетная запись отключена.'
        else:
            error = u'Неверный логин или пароль.'
        return error


class LoginView(LoginBaseView):

    def get(self, request):
        flush_messages(request)
        return TemplateResponse(request, 'enter_lk.html', {})

    def post(self, request):
        error = self.try_login(request)
        if not error:
            return redirect(reverse('exercises:trainers'))
        messages.error(request, error)
        return TemplateResponse(request, 'enter_lk.html', {})


login_view = LoginView.as_view()


def login_vk_view(request):
    if request.user.username:
        user = request.user
        login(request, user)
        if (user.weight == 0) or (user.growth == 0) or (user.age == 0):
            return redirect(reverse('accounts:profile_vk'))
        else:
            return redirect(reverse('exercises:trainers_vk'))
    else:
        return redirect(reverse('exercises:trainers_vk'))


class ProfileView(View):

    def get(self, request):
        flush_messages(request)
        form = UserForm(instance=request.user)
        return TemplateResponse(request, 'edit_profile.html', {
            'user': request.user,
            'day': get_day_user_in_system(request.user),
            'site_url': settings.SITE_URL,
            'form': form,
        })

    def post(self, request):
        flush_messages(request)
        form = UserForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            if request.FILES.get('avatar'):
                user = request.user
                user.avatar = request.FILES.get('avatar')
                user.save()

            form.save()
            msg = 'Изменения сохранены.'
            messages.info(request, msg)
            return redirect(reverse('exercises:trainers'))
        return TemplateResponse(request, 'edit_profile.html', {
            'user': request.user,
            'day': get_day_user_in_system(request.user),
            'site_url': settings.SITE_URL,
            'form': form,
        })


profile_view = ProfileView.as_view()


class ProfileVkView(View):

    def get(self, request, **kwargs):
        flush_messages(request)
        form = UserForm(instance=request.user)
        return TemplateResponse(request, 'edit_profile.html', {
            'user': request.user,
            'day': get_day_user_in_system(request.user),
            'vk': True,
            'site_url': settings.SITE_URL,
            'form': form,
        })

    def post(self, request):
        flush_messages(request)
        form = UserForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            if request.FILES.get('avatar'):
                user = request.user
                user.avatar = request.FILES.get('avatar')
                user.save()

            form.save()
            msg = 'Изменения сохранены.'
            messages.info(request, msg)
            return redirect(reverse('accounts:profile_vk'))
        return TemplateResponse(request, 'edit_profile.html', {
            'user': request.user,
            'vk': True,
            'day': get_day_user_in_system(request.user),
            'site_url': settings.SITE_URL,
            'form': form,
        })


profile_view_vk = ProfileVkView.as_view()


def feedback(request):
    def send_feedback(user, name, message):
        email = settings.FEEDBACK_EMAIL
        subj = 'Форма обратной связи'
        msg = '%s\n%s\n%s' % (user.username, name, message)

        send_email(user, subj, msg)

    flush_messages(request)
    form = FeedbackForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            name = form.cleaned_data.get('name')
            message = form.cleaned_data.get('message')

            try:
                send_feedback(request.user, name, message)
            except Exception, ex:
                msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
                messages.error(request, msg)
            else:
                msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации учётной записи пользователя.'
                messages.info(request, msg)

        else:
            msg = u'Не правильно заполнена форма:' + u','.join(form.errors.values()[0])
            messages.error(request, msg)

    return redirect(request.GET.get('next', '/'))


def registration(request):

    def send_activation_email(user, original_password):
        subj = 'Активация аккаунта.'
        activation_url = 'http://%s/activation/%s/' % (request.get_host(), user.activation_key)
        context = {
            'user': user,
            'password': original_password,
            'activation_url': activation_url
        }
        msg = render_to_string('email/activation.html', context)
        send_email(user, subj, msg)

    flush_messages(request)
    resp = {}
    form = RegistrationForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['email'],
                form.cleaned_data['email'],
                form.cleaned_data['password'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                city=form.cleaned_data['city'],
                age=form.cleaned_data['age'],
                sex=form.cleaned_data['sex'],
                weight=form.cleaned_data['weight'],
                growth=form.cleaned_data['growth'],
            )
            user.is_active = False
            user.save()

            try:
                send_activation_email(user, form.cleaned_data['password'])
            except Exception, ex:
                msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
                messages.error(request, msg)
                resp['error'] = msg
            else:
                msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации учётной записи пользователя.'
                messages.info(request, msg)
                resp['info'] = msg
        else:
            msg = u'Не правильно заполнена форма:' + u','.join(form.errors.values()[0])
            messages.error(request, msg)
            resp['error'] = msg

        return JsonResponse(resp)

    return TemplateResponse(request, 'registration.html', {'form': form})


def activation(request, activation_key):

    def send_activation_done_by_email(user):
        subj = 'Активация выполнена.'
        msg = render_to_string('email/activation_done.html', {'user': user})
        send_email(user, subj, msg)

    flush_messages(request)
    try:
        user = User.objects.get(activation_key=activation_key)
    except User.DoesNotExist:
        msg = 'Неверный код активации.'
        messages.error(request, msg)
        return redirect('/')

    if user:
        user.is_active = True
        user.activation_key = generate_activation_code()
        user.save()
        msg = 'Активация выполнена. Учётная запись активна.'
        messages.info(request, msg)

        try:
            send_activation_done_by_email(user)
        except Exception:
            pass

        try:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
        except Exception, ex:
            messages.error('произошла ошибка при входе. попробуйте позже.')
        return HttpResponseRedirect(settings.SITE_URL)
        # return redirect('/trainers/')
    else:
        msg = 'Активация провалена. Воспользуйтесь ссылкой, которая была Вам выслана по почте!'
        messages.error(request, msg)
        return redirect('/login/')


def password_reset(request):
    def send_reset_password_activation_email(user):
        subj = 'Активация нового пароля'
        activation_url = 'http://%s/password/reset/%s/' % (request.get_host(), user.activation_key)
        context = {
            'user': user,
            'activation_url': activation_url
        }
        msg = render_to_string('email/reset_password_activation.html', context)
        send_email(user, subj, msg)

    flush_messages(request)
    if request.method == 'POST':
        email = request.POST.get('email')
        if not email:
            msg = 'Введите email'
            messages.error(request, msg)
            return TemplateResponse(request, 'change_password.html', {})

        try:
            user = User.objects.get(username=email)
        except User.DoesNotExist:
            msg = 'Внимательно вводите email. На введённый вами не зарегистрирован ни один пользователь.'
            messages.error(request, msg)
            return TemplateResponse(request, 'change_password.html', {})
        else:
            if not user:
                msg = 'Введите email'
                messages.error(request, msg)
                return TemplateResponse(request, 'change_password.html', {})

        try:
            send_reset_password_activation_email(user)
        except:
            msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
            messages.error(request, msg)
        else:
            msg = 'На указанный почтовый ящик отправлено письмо с ссылкой для активации нового пароля.'
            messages.info(request, msg)
            return redirect('/login/')

    return TemplateResponse(request, 'change_password.html', {})


def password_reset_done(request, activation_key):
    def send_message(user, password):
        subj = 'Новый пароль.'
        context = {
            'user': user,
            'password': password
        }
        msg = render_to_string('email/reset_password.html', context)
        send_email(user, subj, msg)

    flush_messages(request)
    try:
        user = User.objects.get(activation_key=activation_key)
    except User.DoesNotExist:
        msg = 'Неверный код активации для смены пароля.'
        messages.error(request, msg)
        return redirect('/login/')
    else:
        if not user:
            msg = 'Неверный код активации для смены пароля.'
            messages.error(request, msg)
            return redirect('/login/')

    import random
    new_password = ''.join(
        [random.choice(list('1234567890abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXY')) for x in xrange(8)])
    user.set_password(new_password)
    user.activation_key = generate_activation_code()
    user.save()
    try:
        send_message(user, new_password)
    except:
        msg = 'Произошла ошибка при доставке сообщения с активацией. Свяжитесь с Администратором.'
        messages.error(request, msg)
    else:
        msg = 'На указанный почтовый ящик отправлено письмо с новым паролем.'
        messages.info(request, msg)

    return redirect('/login/')


def send_email(user, subj, msg):
    send_mail(subj, msg, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=msg)


def get_day_user_in_system(user):
    if user.id:
        now_date = datetime.now().date()
        delta = now_date - user.start_traning_date.date()
        return delta.days + 1
    else:
        return 1


from django.shortcuts import render_to_response
from django.template import RequestContext


def handle404(request):
    response = render_to_response('errors/404.html', {}, context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handle500(request):
    response = render_to_response('errors/500.html', {}, context_instance=RequestContext(request))
    response.status_code = 500
    return response
