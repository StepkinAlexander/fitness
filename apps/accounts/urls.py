from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.accounts.views',
    url(r'^registration/$', 'registration', name='registration'),
    url(r'^activation/(?P<activation_key>[\w]+)/$', 'activation', name='activation'),
    url(r'^login/$', 'login_view', name='login'),
    url(r'^login/vk/$', 'home_vk_view', name='home_vk'),
    url(r'^logout/$', 'logout_view', name='logout'),
    url(r'^password/reset/$', 'password_reset', name='password_reset'), 
    url(r'^password/reset/(?P<activation_key>[\w]+)/$', 'password_reset_done', name='password_reset_done'), 
      
    url(r'^profile/$', 'profile_view', name='profile'),
    url(r'^profile/vk/$', 'profile_view_vk', name='profile_vk'),
    url(r'^feedback/$', 'feedback', name='feedback'),
)