# coding: utf-8
import hashlib
import random
import requests
import logging
import os
from io import BytesIO
from urlparse import urlparse
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.files import File


logger = logging.getLogger()


def generate_activation_code():
    return hashlib.sha1(str(random.random())).hexdigest()


class User(AbstractUser):
    activation_key = models.CharField(u'activation_key', max_length=50, default=generate_activation_code)
    city = models.CharField(max_length=50, blank=True, default='')
    sex = models.CharField(max_length=1, blank=True, default='f')
    avatar = models.ImageField(u"аватар", upload_to='avatar/%Y/%m/%d', blank=True, max_length=255)
    age = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    growth = models.IntegerField(default=0)
    start_traning_date = models.DateTimeField(auto_now_add=True)
    minutes_online = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    def update_avatar(self, url, *args, **kwargs):
        try:
            response = requests.get(url)
        except IOError as exc:
            logger.exception(exc)
        else:
            if response.status_code == 200:
                buf = BytesIO(response.content)
                buf.size = buf.__sizeof__()
                filename = hashlib.md5(url).hexdigest()
                self.avatar.save(filename, File(buf))

    def get_social_auth_providers(self):
        return [sa.provider for sa in self.social_auth.all()]

    def __unicode__(self):
        return self.username


def get_avatar_from_social_response(backend_name, response):
    if backend_name == 'vk-oauth2':  # vkontakte
        return response.get('photo')
    elif backend_name == 'odnoklassniki-oauth2':  # odnoklassniki
        return response.get('pic_2')
    elif backend_name == 'facebook':  # facebook
        if 'id' in response:
            return 'http://graph.facebook.com/%s/picture?type=large' % response['id']


def save_avatar(backend, user, response, *args, **kwargs):
    if user.avatar:
        return

    avatar_url = get_avatar_from_social_response(backend.name, response)
    if avatar_url:
        try:
            user.update_avatar(avatar_url)
        except Exception as exc:
            logger.exception(exc)
