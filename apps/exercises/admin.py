# coding: utf-8
from django.contrib import admin
from .models import Trainer, Exercise, ExercisesDayIntro, Advice


@admin.register(Trainer)
class TrainerAdmin(admin.ModelAdmin):
    pass


@admin.register(Exercise)
class ExerciseAdmin(admin.ModelAdmin):
    list_display = ('trainer', 'title', 'day', 'repetition_calories_burned', )
    pass


@admin.register(ExercisesDayIntro)
class ExercisesDayIntroAdmin(admin.ModelAdmin):
    list_display = ('trainer', 'day', )
    pass

@admin.register(Advice)
class AdviceAdmin(admin.ModelAdmin):
    list_display = ('day', 'content', )
    pass
