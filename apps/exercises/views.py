# coding: utf-8
from django.template.response import TemplateResponse
from .models import Trainer, Exercise, ExercisesDayIntro, MotivationVideo

from django.conf import settings
DEFAULT_TRAINER = settings.DEFAULT_TRAINER


def get_trainer_day(request, trainer):
    day = get_day_user_in_system(request.user)
    trainer_day = 0
    for d in range(1, day + 1):
        exercises = Exercise.objects.filter(trainer=trainer, day=d)
        progress = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
        if len(progress) > 1:
            trainer_day = d

    exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)
    progress = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
    if progress:  # проверка что последняя запись в дневнике не сегодня
        if progress[len(progress) - 1].action_time.day != datetime.now().day:
            trainer_day += 1

    if trainer_day == 0:
        trainer_day = 1
    elif trainer_day > day:
        trainer_day = day
    return trainer_day


def home(request):
    context = {
        'user': request.user,
        'day': get_day_user_in_system(request.user),
        'trainers': Trainer.objects.all(),
        'site_url': settings.SITE_URL,
    }
    return TemplateResponse(request, 'join_trainer.html', context)


def rules_context(request):
    trainer_id = request.GET.get('trainer_id')
    context = {
        'user': request.user,
        'site_url': settings.SITE_URL,
    }

    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer
    else:
        trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
        context['default'] = trainer

    return context


def rules(request):
    context = rules_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'rules.html', context)


def rules_vk(request, **kwargs):
    context = rules_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'rules.html', context)


def trainers_context(request):
    trainer_id = request.GET.get('trainer_id')
    context = {
        'user': request.user,
        'site_url': settings.SITE_URL,
    }

    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer

    context['day'] = get_day_user_in_system(request.user)
    context['trainers'] = Trainer.objects.all()
    if settings.DEBUG:
        context['debug'] = True

    return context


def trainers(request):
    context = trainers_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'join_trainer.html', context)


def trainers_vk(request, **kwargs):
    context = trainers_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'join_trainer.html', context)


def promo_context(request):
    trainer_id = request.GET.get('trainer_id')
    context = {
        'user': request.user,
        'site_url': settings.SITE_URL,
    }
    if settings.DEBUG:
        context['debug'] = True

    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer

    return context


def promo(request):
    context = promo_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'promo.html', context)


def promo_vk(request, **kwargs):
    context = promo_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'promo.html', context)


def about_context(request):
    trainer_id = request.GET.get('trainer_id')
    context = {
        'user': request.user,
        'day': get_day_user_in_system(request.user),
        'site_url': settings.SITE_URL,
    }

    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer

    return context


def about(request):
    context = about_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'about_product.html', context)


def about_vk(request, **kwargs):
    context = about_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'about_product.html', context)


from datetime import datetime


def get_day_user_in_system(user):
    if user.id:
        now_date = datetime.now().date()
        delta = now_date - user.start_traning_date.date()
        return delta.days + 1
    else:
        return 1


def trainer_exercises_context(request):
    day = get_day_user_in_system(request.user)
    trainer_id = request.GET.get('trainer_id')
    context = {
        'user': request.user,
        'day': day,
        'site_url': settings.SITE_URL,
    }

    if trainer_id:
        trainer = Trainer.objects.get(id=trainer_id)
        context['trainer'] = trainer
    else:
        trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
        context['default'] = trainer

    trainer_day = 1  # get_trainer_day(request, trainer) TODO исправить
    context['trainer_day'] = trainer_day

    if 'last_day' in request.GET:
        last_day = request.GET.get('last_day')
        context['last_day'] = last_day
        exercises = Exercise.objects.filter(trainer=trainer, day=last_day)
    else:
        exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)
    context['exercises'] = exercises

    additional_videos = ExercisesDayIntro.objects.get(trainer=trainer, day=trainer_day)
    context['videos'] = additional_videos

    must_burned = 0
    for ex in exercises:
        must_burned += ex.prescription_sets * ex.prescription_repetitions * ex.repetition_calories_burned
    try:
        must_burned = must_burned * request.user.weight
    except:
        must_burned = must_burned * 60
    context['must_burned'] = int(must_burned)

    motivations = MotivationVideo.objects.filter(trainer=trainer)
    context['motivations'] = motivations

    return context


def trainer_exercises(request):
    context = trainer_exercises_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'training.html', context)


def trainer_exercises_vk(request, **kwargs):
    context = trainer_exercises_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'training.html', context)
