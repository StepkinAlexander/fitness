# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0005_auto_20150415_1254'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trainer',
            old_name='family_name',
            new_name='image',
        ),
    ]
