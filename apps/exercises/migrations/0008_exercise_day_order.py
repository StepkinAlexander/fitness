# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0007_auto_20150416_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='day_order',
            field=models.IntegerField(default=0, verbose_name='\u2116 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f \u0437\u0430 \u0434\u0435\u043d\u044c'),
            preserve_default=True,
        ),
    ]
