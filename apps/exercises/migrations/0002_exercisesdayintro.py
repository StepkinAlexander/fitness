# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExercisesDayIntro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(default=0, verbose_name='N-\u0439 \u0434\u0435\u043d\u044c')),
                ('before', models.CharField(max_length=255, verbose_name='\u0432\u0438\u0434\u0435\u043e \u0432\u0441\u0442\u0430\u0432\u043a\u0430 \u0434\u043e')),
                ('after', models.CharField(max_length=255, verbose_name='\u0432\u0438\u0434\u0435\u043e \u0432\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u043e\u0441\u043b\u0435')),
                ('trainer', models.ForeignKey(to='exercises.Trainer')),
            ],
            options={
                'verbose_name': '\u0412\u0438\u0434\u0435\u043e\u0432\u0441\u0442\u0430\u0432\u043a\u0430 \u0434\u043e \u0438 \u043f\u043e\u0441\u043b\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0439',
                'verbose_name_plural': '\u0412\u0438\u0434\u0435\u043e\u0432\u0441\u0442\u0430\u0432\u043a\u0438 \u0434\u043e \u0438 \u043f\u043e\u0441\u043b\u0435 \u0443\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
    ]
