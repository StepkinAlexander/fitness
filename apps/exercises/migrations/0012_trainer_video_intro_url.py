# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0011_motivationdiaryvideo'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainer',
            name='video_intro_url',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
