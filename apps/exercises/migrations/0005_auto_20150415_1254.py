# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0004_auto_20150415_1238'),
    ]

    operations = [
        migrations.RenameField(
            model_name='advice',
            old_name='anounce',
            new_name='announce',
        ),
    ]
