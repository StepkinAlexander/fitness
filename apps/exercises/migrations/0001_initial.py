# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(default=0, verbose_name='N-\u0439 \u0434\u0435\u043d\u044c')),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('video_url', models.CharField(max_length=255, verbose_name='\u0441\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u0438\u0434\u0435\u043e')),
                ('prescription_sets', models.IntegerField(default=0, verbose_name='\u0440\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u043e\u0432\u0430\u043d\u043e \u043f\u043e\u0434\u0445\u043e\u0434\u043e\u0432')),
                ('prescription_repetitions', models.IntegerField(default=0, verbose_name='\u0440\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u043e\u0432\u0430\u043d\u043e \u043f\u043e\u0432\u0442\u043e\u0440\u043e\u0432')),
                ('repetition_calories_burned', models.DecimalField(default=0, verbose_name='\u0441\u043e\u0436\u0436\u0435\u043d\u043e \u043a\u0430\u043b\u043e\u0440\u0438\u0439 \u0437\u0430 \u043f\u043e\u0432\u0442\u043e\u0440\u0435\u043d\u0438\u0435', max_digits=6, decimal_places=4)),
            ],
            options={
                'verbose_name': '\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0423\u043f\u0440\u0430\u0436\u043d\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trainer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('family_name', models.CharField(max_length=255, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('about_trainer', models.TextField(verbose_name='\u041e \u0442\u0440\u0435\u043d\u0435\u0440\u0435', blank=True)),
                ('about_methodic', models.TextField(verbose_name='\u041e \u043c\u0435\u0442\u043e\u0434\u0438\u043a\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u0440\u0435\u043d\u0435\u0440',
                'verbose_name_plural': '\u0422\u0440\u0435\u043d\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='exercise',
            name='trainer',
            field=models.ForeignKey(to='exercises.Trainer'),
            preserve_default=True,
        ),
    ]
