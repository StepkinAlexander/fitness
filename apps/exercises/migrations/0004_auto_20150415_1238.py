# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0003_advice'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advice',
            options={'ordering': ['id'], 'verbose_name': '\u0421\u043e\u0432\u0435\u0442 \u0434\u043d\u044f', 'verbose_name_plural': '\u0421\u043e\u0432\u0435\u0442\u044b \u0434\u043d\u0435\u0439'},
        ),
        migrations.AddField(
            model_name='advice',
            name='anounce',
            field=models.TextField(verbose_name='\u0410\u043d\u043e\u043d\u0441(\u0434\u043b\u044f \u0434\u043d\u0435\u0432\u043d\u0438\u043a\u0430)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='advice',
            name='content',
            field=models.TextField(verbose_name='\u0421\u043e\u0432\u0435\u0442(\u0434\u043b\u044f \u043f\u043e\u043f\u0430\u043f\u0430)', blank=True),
            preserve_default=True,
        ),
    ]
