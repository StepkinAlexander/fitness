# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0009_motivationvideo'),
    ]

    operations = [
        migrations.CreateModel(
            name='MotivationDietVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_url', models.CharField(max_length=255)),
                ('is_main', models.BooleanField(default=False)),
                ('trainer', models.ForeignKey(to='exercises.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
