# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0006_auto_20150415_1411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainer',
            name='image',
            field=models.CharField(max_length=255, verbose_name='image url'),
            preserve_default=True,
        ),
    ]
