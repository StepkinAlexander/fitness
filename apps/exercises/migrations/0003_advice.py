# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0002_exercisesdayintro'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(default=0, verbose_name='N-\u0439 \u0434\u0435\u043d\u044c')),
                ('content', models.TextField(verbose_name='\u0421\u043e\u0432\u0435\u0442', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
