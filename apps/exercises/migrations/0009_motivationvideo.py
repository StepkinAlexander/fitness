# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0008_exercise_day_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='MotivationVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_url', models.CharField(max_length=255)),
                ('trainer', models.ForeignKey(to='exercises.Trainer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
