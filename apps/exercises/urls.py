from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.exercises.views',
    url(r'^exercises/$', 'trainer_exercises', name='exercises'),
    url(r'^exercises/vk/$', 'trainer_exercises_vk', name='exercises_vk'),
    url(r'^trainers/$', 'trainers', name='trainers'),
    url(r'^trainers/vk/$', 'trainers_vk', name='trainers_vk'),
)
