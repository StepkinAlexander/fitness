# coding: utf-8
from django.db import models


class Trainer(models.Model):
    name = models.CharField(u"Имя", max_length=255)
    image = models.CharField(u"image url", max_length=255)
    about_trainer = models.TextField(u'О тренере', blank=True)
    about_methodic = models.TextField(u'О методике', blank=True)
    video_intro_url = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s" % (self.name)

    class Meta:
        verbose_name = "Тренер"
        verbose_name_plural = "Тренеры"


class Exercise(models.Model):
    trainer = models.ForeignKey(Trainer)
    day = models.IntegerField(u"N-й день", default=0)
    day_order = models.IntegerField(u"№ упражнения за день", default=0)
    title = models.CharField(u"Название", max_length=255)
    description = models.TextField(u"описание", blank=True)
    video_url = models.CharField(u"ссылка на видео", max_length=255)
    prescription_sets = models.IntegerField(u"рекомендовано подходов", default=0)
    prescription_repetitions = models.IntegerField(u"рекомендовано повторов", default=0)
    repetition_calories_burned = models.DecimalField(
        u"сожжено калорий за повторение", default=0, max_digits=6, decimal_places=4)

    def __unicode__(self):
        return u'%s. %s. День %s' % (self.trainer, self.title, self.day)

    class Meta:
        verbose_name = "Упражнение"
        verbose_name_plural = "Упражнения"


class ExercisesDayIntro(models.Model):
    trainer = models.ForeignKey(Trainer)
    day = models.IntegerField(u"N-й день", default=0)
    before = models.CharField(u"видео вставка до", max_length=255)
    after = models.CharField(u"видео вставка после", max_length=255)

    def __unicode__(self):
        return u'%s. День %s' % (self.trainer, self.day)

    class Meta:
        verbose_name = "Видеовставка до и после упражнений"
        verbose_name_plural = "Видеовставки до и после упражнений"


class Advice(models.Model):
    day = models.IntegerField(u"N-й день", default=0)
    announce = models.TextField(u"Анонс(для дневника)", blank=True)
    content = models.TextField(u"Совет(для попапа)", blank=True)

    def __unicode__(self):
        return u'День %s. Совет: %s' % (self.day, self.content[:50])

    class Meta:
        verbose_name = "Совет дня"
        verbose_name_plural = "Советы дней"
        ordering = ['id', ]


class MotivationVideo(models.Model):
    trainer = models.ForeignKey(Trainer)
    video_url = models.CharField(max_length=255)


class MotivationDietVideo(models.Model):
    trainer = models.ForeignKey(Trainer)
    video_url = models.CharField(max_length=255)
    is_main = models.BooleanField(default=False)

class MotivationDiaryVideo(models.Model):
    trainer = models.ForeignKey(Trainer)
    video_url = models.CharField(max_length=255)
