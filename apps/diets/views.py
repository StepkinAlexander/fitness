# coding: utf-8
from django.template.response import TemplateResponse
from django.conf import settings


from apps.accounts.views import get_day_user_in_system
from apps.exercises.models import Exercise, Trainer, MotivationDietVideo
from .models import Diet
from apps.progress.models import ExerciseProgress

from django.conf import settings
DEFAULT_TRAINER = settings.DEFAULT_TRAINER

from datetime import datetime


def get_diet(day):
    if day > 0:
        diet = Diet.objects.get(day=day)
    else:
        diet = Diet.objects.all()[0]
    return diet


def get_trainer_day(request, trainer):
    day = get_day_user_in_system(request.user)
    trainer_day = 0
    for d in range(1, day + 1):
        exercises = Exercise.objects.filter(trainer=trainer, day=d)
        progress = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
        if len(progress) > 1:
            trainer_day = d

    exercises = Exercise.objects.filter(trainer=trainer, day=trainer_day)
    progress = ExerciseProgress.objects.filter(user=request.user, exercise__in=exercises)
    if progress:  # проверка что последняя запись в дневнике не сегодня
        if progress[len(progress) - 1].action_time.day != datetime.now().day:
            trainer_day += 1

    if trainer_day == 0:
        trainer_day = 1
    elif trainer_day > day:
        trainer_day = day
    return trainer_day


def diet_context(request):
    if request.user.username:
        if 'last_day' in request.GET:
            day = request.GET.get('last_day')
        else:
            day = get_day_user_in_system(request.user) % 14 + 1
        trainer_id = request.GET.get('trainer_id')
        context = {
            'user': request.user,
            'day': day,
            'diet': get_diet(day),
            'site_url': settings.SITE_URL,
        }
        if 'last_day' in request.GET:
            context['last_day'] = day

        if trainer_id:
            trainer = Trainer.objects.get(id=trainer_id)
            context['trainer'] = trainer

            context['trainer_day'] = get_trainer_day(request, trainer)
        else:
            trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
            context['default'] = trainer
    else:  # для неавторизованных
        trainer_id = request.GET.get('trainer_id')
        context = {
            'user': request.user,
            'day': 1,
            'diet': get_diet(1),
            'site_url': settings.SITE_URL,
        }
        if trainer_id:
            trainer = Trainer.objects.get(id=trainer_id)
            context['trainer'] = trainer
            context['trainer_day'] = 1
        else:
            trainer = Trainer.objects.get(id=DEFAULT_TRAINER)
            context['default'] = trainer

    videos = {
        'first': MotivationDietVideo.objects.get(trainer=trainer, is_main=True),
        'other': MotivationDietVideo.objects.filter(trainer=trainer, is_main=False),
    }
    context['videos'] = videos

    return context


def get_diet_today(request):
    context = diet_context(request)
    context['share_url'] = settings.SITE_SHARE_URL

    return TemplateResponse(request, 'program.html', context)


def get_diet_today_vk(request, **kwargs):
    context = diet_context(request)
    context['vk'] = True
    context['vk_share_url'] = settings.VK_SHARE_URL

    return TemplateResponse(request, 'program.html', context)
