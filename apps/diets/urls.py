from django.conf.urls import patterns, include, url

urlpatterns = patterns('apps.diets.views',
    url(r'^diet/$', 'get_diet_today', name='diet_today'),
    url(r'^diet/vk/$', 'get_diet_today_vk', name='diet_today_vk'),
)