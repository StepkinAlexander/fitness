# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diets', '0002_motivationdietvideo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motivationdietvideo',
            name='trainer',
        ),
        migrations.DeleteModel(
            name='MotivationDietVideo',
        ),
    ]
