# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Diet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(default=0, verbose_name='N-\u0439 \u0434\u0435\u043d\u044c')),
                ('recommend', models.TextField(verbose_name='\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0430\u0446\u0438\u0438', blank=True)),
                ('eat1', models.TextField(verbose_name='\u0417\u0430\u0432\u0442\u0440\u0430\u043a', blank=True)),
                ('eat2', models.TextField(verbose_name='\u041b\u0430\u043d\u0447', blank=True)),
                ('eat3', models.TextField(verbose_name='\u041e\u0431\u0435\u0434', blank=True)),
                ('eat4', models.TextField(verbose_name='\u041f\u043e\u043b\u0434\u043d\u0438\u043a', blank=True)),
                ('eat5', models.TextField(verbose_name='\u0423\u0436\u0438\u043d', blank=True)),
            ],
            options={
                'ordering': ['day'],
                'verbose_name': '\u0414\u0438\u0435\u0442\u0430',
                'verbose_name_plural': '\u0414\u0438\u0435\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DietViewed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('viewed', models.BooleanField(default=False, verbose_name='\u0434\u0438\u0435\u0442\u0430 \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u043d\u0430')),
                ('diet', models.ForeignKey(to='diets.Diet')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['user', 'diet'],
                'verbose_name': '\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0430 \u0434\u0438\u0435\u0442\u044b',
                'verbose_name_plural': '\u0421\u0442\u0430\u0442\u0443\u0441\u044b \u043f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0430 \u0434\u0438\u0435\u0442',
            },
            bases=(models.Model,),
        ),
    ]
