# coding: utf-8
from django.db import models
from apps.accounts.models import User


class Diet(models.Model):
    day = models.IntegerField(u"N-й день", default=0)
    recommend = models.TextField(u'Рекомендации', blank=True)
    eat1 = models.TextField(u'Завтрак', blank=True)
    eat2 = models.TextField(u'Ланч', blank=True)
    eat3 = models.TextField(u'Обед', blank=True)
    eat4 = models.TextField(u'Полдник', blank=True)
    eat5 = models.TextField(u'Ужин', blank=True)

    def __unicode__(self):
        return u'день %s' % self.day

    class Meta:
        verbose_name = "Диета"
        verbose_name_plural = "Диеты"
        ordering = ['day']


class DietViewed(models.Model):
    diet = models.ForeignKey(Diet)
    user = models.ForeignKey(User)
    viewed = models.BooleanField(u"диета просмотрена", default=False)

    def __unicode__(self):
        return u'%s, день %s. Статус просмотра диеты: %s' % (self.user.username, self.diet.day, self.viewed)

    class Meta:
        verbose_name = "Статус просмотра диеты"
        verbose_name_plural = "Статусы просмотра диет"
        ordering = ['user', 'diet']
