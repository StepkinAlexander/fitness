# coding: utf-8
from django.contrib import admin
from .models import Diet, DietViewed


@admin.register(Diet)
class DietAdmin(admin.ModelAdmin):
    list_display = ('day', 'recommend', )

# @admin.register(DietViewed)
# class DietViewedAdmin(admin.ModelAdmin):
#     list_display = ('user', 'diet', 'viewed', )
